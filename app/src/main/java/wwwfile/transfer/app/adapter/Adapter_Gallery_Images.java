package wwwfile.transfer.app.adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import java.io.File;
import java.util.List;

import wwwfile.transfer.app.filetransferapp.MainActivity;
import wwwfile.transfer.app.filetransferapp.R;
import wwwfile.transfer.app.views.SquareAppCompatImageView;

public class Adapter_Gallery_Images extends RecyclerView.Adapter<Adapter_Gallery_Images.Adapter_GalleryImage_holder> {
    private AppCompatActivity activity;
    private List<String> allimages;

    public Adapter_Gallery_Images(AppCompatActivity activity, List<String> allimages) {
        this.activity = activity;
        this.allimages = allimages;
    }

    @Override
    public Adapter_GalleryImage_holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_thumbnails, parent, false);
        Adapter_GalleryImage_holder ret = new Adapter_GalleryImage_holder(v);
        return ret;
    }

    @Override
    public void onBindViewHolder(Adapter_GalleryImage_holder holder, int position) {
        final String imagepath = getItem(position);
        final File ff=new File(imagepath);
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 8;
        Bitmap bitmap=BitmapFactory.decodeFile(ff.getAbsolutePath(),options);
        holder.gallery_icon.setImageBitmap(bitmap);
        holder.gallery_selecter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    MainActivity.addItemtoList(ff.getAbsolutePath());
                } else {
                    MainActivity.removeItemFromList(ff.getAbsolutePath());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return allimages.size();
    }

    public String getItem(int position) {
        return allimages.get(position);
    }

    public static class Adapter_GalleryImage_holder extends RecyclerView.ViewHolder {
        public SquareAppCompatImageView gallery_icon;
        public AppCompatCheckBox gallery_selecter;
        public Adapter_GalleryImage_holder(View v) {
            super(v);
            gallery_icon = (SquareAppCompatImageView) v.findViewById(R.id.gallery_icon);
            gallery_selecter=(AppCompatCheckBox)v.findViewById(R.id.gallery_selecter);
        }
    }
}