package wwwfile.transfer.app.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import wwwfile.transfer.app.models.FragmentTitles;

public class Adapter_ShowDataINTabs extends FragmentPagerAdapter {
    private List<FragmentTitles> fragments;
    public Adapter_ShowDataINTabs(FragmentManager fm,List<FragmentTitles> fragments) {
        super(fm);
        this.fragments=fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position).getFragment();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragments.get(position).getTitle();
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
