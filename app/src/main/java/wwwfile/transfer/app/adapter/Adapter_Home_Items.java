package wwwfile.transfer.app.adapter;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import wwwfile.transfer.app.filetransferapp.R;
import wwwfile.transfer.app.models.HomeItemIcon;

public class Adapter_Home_Items extends RecyclerView.Adapter<Adapter_Home_Items.Holder_Home_items> {
    public AppCompatActivity activity;
    public List<HomeItemIcon> allItems;
    public Adapter_Home_Items(AppCompatActivity activity, List<HomeItemIcon> allItems) {
        this.activity = activity;
        this.allItems = allItems;
    }

    @Override
    public Holder_Home_items onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_item_icon, parent, false);
        Holder_Home_items ret = new Holder_Home_items(v);
        return ret;
    }

    @Override
    public void onBindViewHolder(Holder_Home_items holder, int position) {
        final HomeItemIcon item=allItems.get(position);
        holder.icon_item.setImageResource(item.getIcon());
        holder.text_item.setText(item.getText().toString());
        holder.itemView.setOnClickListener(item.getAction());
    }

    @Override
    public int getItemCount() {
        return allItems.size();
    }

    public class Holder_Home_items extends RecyclerView.ViewHolder{
        public AppCompatImageView icon_item;
        public AppCompatTextView text_item;
        public Holder_Home_items(View v) {
            super(v);
            icon_item=(AppCompatImageView)v.findViewById(R.id.icon_item);
            text_item=(AppCompatTextView)v.findViewById(R.id.text_item);
        }
    }

}
