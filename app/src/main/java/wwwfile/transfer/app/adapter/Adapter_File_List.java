package wwwfile.transfer.app.adapter;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import java.io.File;
import java.util.List;

import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.filetransferapp.MainActivity;
import wwwfile.transfer.app.filetransferapp.R;

public class Adapter_File_List extends RecyclerView.Adapter<Adapter_File_List.Adapter_FileItem_holder> {
    private AppCompatActivity activity;
    private List<File> allfiles;

    public Adapter_File_List(AppCompatActivity activity, List<File> allfiles) {
        this.activity = activity;
        this.allfiles = allfiles;
    }

    @Override
    public Adapter_FileItem_holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.file_item, parent, false);
        Adapter_FileItem_holder ret = new Adapter_FileItem_holder(v);
        return ret;
    }
    @Override
    public void onBindViewHolder(Adapter_FileItem_holder holder, int position) {
        final File item = getItem(position);
        holder.size_file.setText(Constants.ConvertSizeInStandard(item.length()));
        holder.title_file.setText(item.getName());
        holder.file_selecter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    MainActivity.addItemtoList(item.getAbsolutePath());
                }else {
                    MainActivity.removeItemFromList(item.getAbsolutePath());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return allfiles.size();
    }

    public File getItem(int position) {
        return allfiles.get(position);
    }

    public static class Adapter_FileItem_holder extends RecyclerView.ViewHolder {
        public AppCompatTextView title_file;
        public AppCompatTextView size_file;
        public AppCompatCheckBox file_selecter;
        public Adapter_FileItem_holder(View v) {
            super(v);
            title_file = (AppCompatTextView) v.findViewById(R.id.title_file);
            size_file = (AppCompatTextView) v.findViewById(R.id.size_file);
            file_selecter = (AppCompatCheckBox) v.findViewById(R.id.file_selecter);
        }
    }
}
