package wwwfile.transfer.app.adapter;

import android.net.wifi.p2p.WifiP2pDevice;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.filetransferapp.R;
import wwwfile.transfer.app.interfaces.DeviceActionListener;

public class Adapter_PeersRecycler extends RecyclerView.Adapter<Adapter_PeersRecycler.Adapter_PeerHolder> {
    public AppCompatActivity activity;
    public List<WifiP2pDevice> allDevices;

    public Adapter_PeersRecycler(AppCompatActivity activity, List<WifiP2pDevice> allDevices) {
        this.activity = activity;
        this.allDevices = allDevices;
    }
    @Override
    public Adapter_PeerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_devices, parent, false);
        Adapter_PeerHolder ret= new Adapter_PeerHolder(v);
        return ret;
    }
    @Override
    public void onBindViewHolder(Adapter_PeerHolder holder, final int position) {
        final WifiP2pDevice device=getItem(position);
        if (device != null) {
            if (holder.device_name != null) {
                holder.device_name.setText(device.deviceName);
            }
            if (holder.device_details != null) {
                holder.device_details.setText(Constants.getDeviceStatus(device.status));
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((DeviceActionListener)Adapter_PeersRecycler.this.activity).showDetails(device);
                }
            });
        }
    }
    @Override
    public int getItemCount() {
        return allDevices.size();
    }


    public class Adapter_PeerHolder extends RecyclerView.ViewHolder{
        public AppCompatImageView device_icon;
        public AppCompatTextView device_details,device_name;
        public Adapter_PeerHolder(View v) {
            super(v);
            device_icon=(AppCompatImageView)v.findViewById(R.id.device_icon);
            device_details=(AppCompatTextView)v.findViewById(R.id.device_details);
            device_name=(AppCompatTextView)v.findViewById(R.id.device_name);
        }
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    public WifiP2pDevice getItem(int position)
    {
        return this.allDevices.get(position);
    }
}
