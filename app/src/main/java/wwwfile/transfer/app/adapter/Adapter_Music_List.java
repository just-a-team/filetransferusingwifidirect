package wwwfile.transfer.app.adapter;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import java.io.File;
import java.util.List;

import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.filetransferapp.MainActivity;
import wwwfile.transfer.app.filetransferapp.R;
import wwwfile.transfer.app.models.MusicItem;

public class Adapter_Music_List extends RecyclerView.Adapter<Adapter_Music_List.Adapter_MusicItem_holder> {
    private AppCompatActivity activity;
    private List<MusicItem> allsongs;

    public Adapter_Music_List(AppCompatActivity activity, List<MusicItem> allsongs) {
        this.activity = activity;
        this.allsongs = allsongs;
    }

    @Override
    public Adapter_MusicItem_holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.music_item, parent, false);
        Adapter_MusicItem_holder ret = new Adapter_MusicItem_holder(v);
        return ret;
    }
    @Override
    public void onBindViewHolder(Adapter_MusicItem_holder holder, int position) {
        final MusicItem item = getItem(position);
        holder.size_song.setText(Constants.ConvertSizeInStandard(Long.parseLong(item.getSize())));
        holder.title_song.setText(item.getTitle());
        holder.music_selecter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String dd=item.getData();
                File ff=new File(dd);
                if (isChecked) {
                    MainActivity.addItemtoList(ff.getAbsolutePath());
                } else {
                    MainActivity.removeItemFromList(ff.getAbsolutePath());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return allsongs.size();
    }

    public MusicItem getItem(int position) {
        return allsongs.get(position);
    }

    public static class Adapter_MusicItem_holder extends RecyclerView.ViewHolder {
        public AppCompatTextView title_song;
        public AppCompatTextView size_song;
        public AppCompatCheckBox music_selecter;
        public Adapter_MusicItem_holder(View v) {
            super(v);
            title_song = (AppCompatTextView) v.findViewById(R.id.title_song);
            size_song = (AppCompatTextView) v.findViewById(R.id.size_song);
            music_selecter=(AppCompatCheckBox)v.findViewById(R.id.music_selecter);
        }
    }
}