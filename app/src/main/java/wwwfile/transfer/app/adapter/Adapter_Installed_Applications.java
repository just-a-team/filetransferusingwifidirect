package wwwfile.transfer.app.adapter;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import java.io.File;
import java.util.List;

import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.filetransferapp.R;
import wwwfile.transfer.app.views.SquareAppCompatImageView;

public class Adapter_Installed_Applications extends RecyclerView.Adapter<Adapter_Installed_Applications.Adapter_Application_holder> {
    private AppCompatActivity activity;
    private PackageManager pm;
    private List<PackageInfo> allapps;

    public Adapter_Installed_Applications(AppCompatActivity activity, PackageManager pm, List<PackageInfo> allapps) {
        this.activity = activity;
        this.pm = pm;
        this.allapps = allapps;
    }

    @Override
    public Adapter_Application_holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.app_icon, parent, false);
        Adapter_Application_holder ret= new Adapter_Application_holder(v);
        return ret;
    }

    @Override
    public void onBindViewHolder(Adapter_Application_holder holder, int position) {
        final PackageInfo device=getItem(position);
        if (device != null) {
            if (holder.app_name != null) {
                holder.app_name.setText(device.applicationInfo.name);
            }
            if (holder.app_size != null) {
                try {
                    holder.app_size.setText(Constants.ConvertSizeInStandard(new File((activity.getPackageManager().getApplicationInfo(device.packageName, -1)).sourceDir).length()));
                } catch (PackageManager.NameNotFoundException e) {
                    Log.e(Constants.TAG,e.getMessage());
                }

            }
            if(holder.app_icon!=null) {
                holder.app_icon.setImageDrawable(device.applicationInfo.loadIcon(this.pm));
            }
        }
        holder.app_selecter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                /*if (isChecked) {
                    MainActivity.addItemtoList(device);
                } else {
                    MainActivity.removeItemFromList(ff.getAbsolutePath());
                }*/
            }
        });
    }
    @Override
    public int getItemCount() {
        return allapps.size();
    }
    public PackageInfo getItem(int position)
    {
        return allapps.get(position);
    }
    public static class Adapter_Application_holder extends RecyclerView.ViewHolder{
        public SquareAppCompatImageView app_icon;
        public AppCompatTextView app_name,app_size;
        public AppCompatCheckBox app_selecter;
        public Adapter_Application_holder(View v) {
            super(v);
            app_icon=(SquareAppCompatImageView)v.findViewById(R.id.app_icon);
            app_name=(AppCompatTextView)v.findViewById(R.id.app_name);
            app_size=(AppCompatTextView)v.findViewById(R.id.app_size);
            app_selecter=(AppCompatCheckBox)v.findViewById(R.id.app_selecter);
        }
    }
}
