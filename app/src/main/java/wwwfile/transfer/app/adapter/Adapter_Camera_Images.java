package wwwfile.transfer.app.adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import java.io.File;
import java.util.List;

import wwwfile.transfer.app.filetransferapp.MainActivity;
import wwwfile.transfer.app.filetransferapp.R;
import wwwfile.transfer.app.views.SquareAppCompatImageView;

public class Adapter_Camera_Images extends RecyclerView.Adapter<Adapter_Camera_Images.Adapter_CameraImage_holder> {
    private AppCompatActivity activity;
    private List<String> allimages;

    public Adapter_Camera_Images(AppCompatActivity activity, List<String> allimages) {
        this.activity = activity;
        this.allimages = allimages;
    }

    @Override
    public Adapter_CameraImage_holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_thumbnail, parent, false);
        Adapter_CameraImage_holder ret = new Adapter_CameraImage_holder(v);
        return ret;
    }

    @Override
    public void onBindViewHolder(Adapter_CameraImage_holder holder, int position) {
        final String imagepath = getItem(position);
        final File ff=new File(imagepath);

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 8;
        Bitmap bitmap=BitmapFactory.decodeFile(ff.getAbsolutePath(),options);
        holder.image_icon.setImageBitmap(bitmap);
        holder.image_selecter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    MainActivity.addItemtoList(ff.getAbsolutePath());
                } else {
                    MainActivity.removeItemFromList(ff.getAbsolutePath());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return allimages.size();
    }

    public String getItem(int position) {
        return allimages.get(position);
    }

    public static class Adapter_CameraImage_holder extends RecyclerView.ViewHolder {
        public SquareAppCompatImageView image_icon;
        public AppCompatCheckBox image_selecter;
        public Adapter_CameraImage_holder(View v) {
            super(v);
            image_icon = (SquareAppCompatImageView) v.findViewById(R.id.image_icon);
            image_selecter=(AppCompatCheckBox)v.findViewById(R.id.image_selecter);
        }
    }
}