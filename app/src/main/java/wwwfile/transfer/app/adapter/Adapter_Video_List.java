package wwwfile.transfer.app.adapter;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import java.io.File;
import java.util.List;

import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.filetransferapp.MainActivity;
import wwwfile.transfer.app.filetransferapp.R;
import wwwfile.transfer.app.models.VideoItem;

public class Adapter_Video_List extends RecyclerView.Adapter<Adapter_Video_List.Adapter_VideoItem_holder> {
    private AppCompatActivity activity;
    private List<VideoItem> allsongs;

    public Adapter_Video_List(AppCompatActivity activity, List<VideoItem> allsongs) {
        this.activity = activity;
        this.allsongs = allsongs;
    }

    @Override
    public Adapter_VideoItem_holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_item, parent, false);
        Adapter_VideoItem_holder ret = new Adapter_VideoItem_holder(v);
        return ret;
    }
    @Override
    public void onBindViewHolder(Adapter_VideoItem_holder holder, int position) {
        final VideoItem item = getItem(position);
        holder.size_video.setText(Constants.ConvertSizeInStandard(Long.parseLong(item.getSize())));
        holder.title_video.setText(item.getTitle());
        holder.video_selecter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String dd = item.getData();
                File ff = new File(dd);
                if (isChecked) {
                    MainActivity.addItemtoList(ff.getAbsolutePath());
                } else {
                    MainActivity.removeItemFromList(ff.getAbsolutePath());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return allsongs.size();
    }

    public VideoItem getItem(int position) {
        return allsongs.get(position);
    }

    public static class Adapter_VideoItem_holder extends RecyclerView.ViewHolder {
        public AppCompatTextView title_video;
        public AppCompatTextView size_video;
        public AppCompatCheckBox video_selecter;
        public Adapter_VideoItem_holder(View v) {
            super(v);
            title_video = (AppCompatTextView) v.findViewById(R.id.title_video);
            size_video = (AppCompatTextView) v.findViewById(R.id.size_video);
            video_selecter=(AppCompatCheckBox)v.findViewById(R.id.video_selecter);
        }
    }
}