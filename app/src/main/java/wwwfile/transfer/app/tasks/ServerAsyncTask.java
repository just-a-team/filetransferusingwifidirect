package wwwfile.transfer.app.tasks;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

import wwwfile.transfer.app.base.Constants;

public class ServerAsyncTask extends AsyncTask<Void, Void, String> {
    private final Context context;
    private final TextView statusText;
    private static boolean server_running = false;
    public ServerAsyncTask(Context context, View statusText) {
        this.context = context;
        this.statusText = (TextView) statusText;
    }
    @Override
    protected String doInBackground(Void... params) {
        try {
            ServerSocket serverSocket = new ServerSocket(Constants.PORT);
            Socket client = serverSocket.accept();
            final File f = new File(Environment.getExternalStorageDirectory() + "/" + context.getPackageName() + "/wifip2pshared-" + System.currentTimeMillis() + ".jpg");
            File dirs = new File(f.getParent());
            if (!dirs.exists())
                dirs.mkdirs();
            f.createNewFile();
            InputStream inputstream = client.getInputStream();
            Constants.copyFile(inputstream, new FileOutputStream(f));
            serverSocket.close();
            server_running = false;
            return f.getAbsolutePath();
        } catch (IOException e) {
            Log.e(Constants.TAG, e.getMessage());
            return null;
        }
    }
    @Override
    protected void onPostExecute(String result) {
        if (result != null) {
            statusText.setText("File copied - " + result);
            Intent intent = new Intent();
            intent.setAction(android.content.Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.parse("file://" + result), "image/*");
            context.startActivity(intent);
        }
        server_running=true;
    }
    @Override
    protected void onPreExecute() {
        if (!server_running) {
            statusText.setText("Opening a server socket");
        }else {
            cancel(true);
        }
    }
}