package wwwfile.transfer.app.swiftp.server;

import android.util.Log;

import java.io.File;

import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.swiftp.MediaUpdater;

public class CmdRMD extends FtpCmd implements Runnable {
    protected String input;

    public CmdRMD(SessionThread sessionThread, String input) {
        super(sessionThread);
        this.input = input;
    }

    @Override
    public void run() {
        Log.d(Constants.TAG, "RMD executing");
        String param = getParameter(input);
        File toRemove;
        String errString = null;
        mainblock: {
            if (param.length() < 1) {
                errString = "550 Invalid argument\r\n";
                break mainblock;
            }
            toRemove = inputPathToChrootedFile(sessionThread.getWorkingDir(), param);
            if (violatesChroot(toRemove)) {
                errString = "550 Invalid name or chroot violation\r\n";
                break mainblock;
            }
            if (!toRemove.isDirectory()) {
                errString = "550 Can't RMD a non-directory\r\n";
                break mainblock;
            }
            if (toRemove.equals(new File("/"))) {
                errString = "550 Won't RMD the root directory\r\n";
                break mainblock;
            }
            if (!recursiveDelete(toRemove)) {
                errString = "550 Deletion error, possibly incomplete\r\n";
                break mainblock;
            }
        }
        if (errString != null) {
            sessionThread.writeString(errString);
            Log.i(Constants.TAG, "RMD failed: " + errString.trim());
        } else {
            sessionThread.writeString("250 Removed directory\r\n");
        }
        Log.d(Constants.TAG, "RMD finished");
    }
    protected boolean recursiveDelete(File toDelete) {
        if (!toDelete.exists()) {
            return false;
        }
        if (toDelete.isDirectory()) {
            boolean success = true;
            for (File entry : toDelete.listFiles()) {
                success &= recursiveDelete(entry);
            }
            Log.d(Constants.TAG, "Recursively deleted: " + toDelete);
            return success && toDelete.delete();
        } else {
            Log.d(Constants.TAG, "RMD deleting file: " + toDelete);
            boolean success = toDelete.delete();
            MediaUpdater.notifyFileDeleted(toDelete.getPath());
            return success;
        }
    }
}
