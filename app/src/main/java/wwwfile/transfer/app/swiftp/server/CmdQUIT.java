package wwwfile.transfer.app.swiftp.server;

import android.util.Log;

import wwwfile.transfer.app.base.Constants;

public class CmdQUIT extends FtpCmd implements Runnable {
    public CmdQUIT(SessionThread sessionThread, String input) {
        super(sessionThread);
    }

    @Override
    public void run() {
        Log.d(Constants.TAG, "QUIT executing");
        sessionThread.writeString("221 Goodbye\r\n");
        sessionThread.closeSocket();
    }

}
