package wwwfile.transfer.app.swiftp.server;

import android.util.Log;

import java.io.File;
import java.lang.reflect.Constructor;

import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.swiftp.FsSettings;

public abstract class FtpCmd implements Runnable {
    protected SessionThread sessionThread;

    protected static CmdMap[] cmdClasses = { new CmdMap("SYST", CmdSYST.class),
            new CmdMap("USER", CmdUSER.class), new CmdMap("PASS", CmdPASS.class),
            new CmdMap("TYPE", CmdTYPE.class), new CmdMap("CWD", CmdCWD.class),
            new CmdMap("PWD", CmdPWD.class), new CmdMap("LIST", CmdLIST.class),
            new CmdMap("PASV", CmdPASV.class), new CmdMap("RETR", CmdRETR.class),
            new CmdMap("NLST", CmdNLST.class), new CmdMap("NOOP", CmdNOOP.class),
            new CmdMap("STOR", CmdSTOR.class), new CmdMap("DELE", CmdDELE.class),
            new CmdMap("RNFR", CmdRNFR.class), new CmdMap("RNTO", CmdRNTO.class),
            new CmdMap("RMD", CmdRMD.class), new CmdMap("MKD", CmdMKD.class),
            new CmdMap("OPTS", CmdOPTS.class), new CmdMap("PORT", CmdPORT.class),
            new CmdMap("QUIT", CmdQUIT.class), new CmdMap("FEAT", CmdFEAT.class),
            new CmdMap("SIZE", CmdSIZE.class), new CmdMap("CDUP", CmdCDUP.class),
            new CmdMap("APPE", CmdAPPE.class), new CmdMap("XCUP", CmdCDUP.class),
            new CmdMap("XPWD", CmdPWD.class),
            new CmdMap("XMKD", CmdMKD.class),
            new CmdMap("XRMD", CmdRMD.class),
            new CmdMap("MDTM", CmdMDTM.class),
            new CmdMap("MFMT", CmdMFMT.class),
            new CmdMap("REST", CmdREST.class),
            new CmdMap("SITE", CmdSITE.class),
    };

    private static Class<?>[] allowedCmdsWhileAnonymous = { CmdUSER.class, CmdPASS.class,
            CmdCWD.class, CmdLIST.class, CmdMDTM.class, CmdNLST.class, CmdPASV.class,
            CmdPWD.class, CmdQUIT.class, CmdRETR.class, CmdSIZE.class, CmdTYPE.class,
            CmdCDUP.class, CmdNOOP.class, CmdSYST.class, CmdPORT.class,
    };

    public FtpCmd(SessionThread sessionThread) {
        this.sessionThread = sessionThread;
    }

    @Override
    abstract public void run();

    protected static void dispatchCommand(SessionThread session, String inputString) {
        String[] strings = inputString.split(" ");
        String unrecognizedCmdMsg = "502 Command not recognized\r\n";
        if (strings == null) {
            String errString = "502 Command parse error\r\n";
            Log.d(Constants.TAG, errString);
            session.writeString(errString);
            return;
        }
        if (strings.length < 1) {
            Log.d(Constants.TAG, "No strings parsed");
            session.writeString(unrecognizedCmdMsg);
            return;
        }
        String verb = strings[0];
        if (verb.length() < 1) {
            Log.i(Constants.TAG, "Invalid command verb");
            session.writeString(unrecognizedCmdMsg);
            return;
        }
        FtpCmd cmdInstance = null;
        verb = verb.trim();
        verb = verb.toUpperCase();
        for (int i = 0; i < cmdClasses.length; i++) {

            if (cmdClasses[i].getName().equals(verb)) {
                Constructor<? extends FtpCmd> constructor;
                try {
                    constructor = cmdClasses[i].getCommand().getConstructor(
                            new Class[] { SessionThread.class, String.class });
                } catch (NoSuchMethodException e) {
                    Log.e(Constants.TAG, "FtpCmd subclass lacks expected " + "constructor ");
                    return;
                }
                try {
                    cmdInstance = constructor.newInstance(new Object[] { session,
                            inputString });
                } catch (Exception e) {
                    Log.e(Constants.TAG, "Instance creation error on FtpCmd");
                    return;
                }
            }
        }
        if (cmdInstance == null) {
            Log.d(Constants.TAG, "Ignoring unrecognized FTP verb: " + verb);
            session.writeString(unrecognizedCmdMsg);
            return;
        }

        if (session.isUserLoggedIn()) {
            cmdInstance.run();
        } else if (session.isAnonymouslyLoggedIn() == true) {
            boolean validCmd = false;
            for (Class<?> cl : allowedCmdsWhileAnonymous) {
                if (cmdInstance.getClass().equals(cl)) {
                    validCmd = true;
                    break;
                }
            }
            if (validCmd == true) {
                cmdInstance.run();
            } else {
                session.writeString("530 Guest user is not allowed to use that command\r\n");
            }
        } else if (cmdInstance.getClass().equals(CmdUSER.class)
                || cmdInstance.getClass().equals(CmdPASS.class)
                || cmdInstance.getClass().equals(CmdQUIT.class)) {
            cmdInstance.run();
        } else {
            session.writeString("530 Login first with USER and PASS, or QUIT\r\n");
        }
    }
    static public String getParameter(String input, boolean silent) {
        if (input == null) {
            return "";
        }
        int firstSpacePosition = input.indexOf(' ');
        if (firstSpacePosition == -1) {
            return "";
        }
        String retString = input.substring(firstSpacePosition + 1);
        retString = retString.replaceAll("\\s+$", "");

        if (!silent) {
            Log.d(Constants.TAG, "Parsed argument: " + retString);
        }
        return retString;
    }
    static public String getParameter(String input) {
        return getParameter(input, false);
    }

    public static File inputPathToChrootedFile(File existingPrefix, String param) {
        try {
            if (param.charAt(0) == '/') {
                File chroot = FsSettings.getChrootDir();
                return new File(chroot, param);
            }
        } catch (Exception e) {
        }
        return new File(existingPrefix, param);
    }

    public boolean violatesChroot(File file) {
        try {
            File chroot = FsSettings.getChrootDir();
            String canonicalChroot = chroot.getCanonicalPath();
            String canonicalPath = file.getCanonicalPath();
            if (!canonicalPath.startsWith(canonicalChroot)) {
                Log.i(Constants.TAG, "Path violated folder restriction, denying");
                Log.d(Constants.TAG, "path: " + canonicalPath);
                Log.d(Constants.TAG, "chroot: " + chroot.toString());
                return true;
            }
            return false;
        } catch (Exception e) {
            Log.i(Constants.TAG, "Path canonicalization problem: " + e.toString());
            Log.i(Constants.TAG, "When checking file: " + file.getAbsolutePath());
            return true;
        }
    }
}
