package wwwfile.transfer.app.swiftp.server;

import android.util.Log;

import java.net.InetAddress;

import wwwfile.transfer.app.base.Constants;

public class CmdPASV extends FtpCmd implements Runnable {
    public CmdPASV(SessionThread sessionThread, String input) {
        super(sessionThread);
    }

    @Override
    public void run() {
        String cantOpen = "502 Couldn't open a port\r\n";
        Log.d(Constants.TAG, "PASV running");
        int port;
        if ((port = sessionThread.onPasv()) == 0) {
            Log.e(Constants.TAG, "Couldn't open a port for PASV");
            sessionThread.writeString(cantOpen);
            return;
        }
        InetAddress addr = sessionThread.getDataSocketPasvIp();
        if (addr == null) {
            Log.e(Constants.TAG, "PASV IP string invalid");
            sessionThread.writeString(cantOpen);
            return;
        }
        Log.d(Constants.TAG, "PASV sending IP: " + addr.getHostAddress());
        if (port < 1) {
            Log.e(Constants.TAG, "PASV port number invalid");
            sessionThread.writeString(cantOpen);
            return;
        }
        StringBuilder response = new StringBuilder("227 Entering Passive Mode (");
        response.append(addr.getHostAddress().replace('.', ','));
        response.append(",");
        response.append(port / 256);
        response.append(",");
        response.append(port % 256);
        response.append(").\r\n");
        String responseString = response.toString();
        sessionThread.writeString(responseString);
        Log.d(Constants.TAG, "PASV completed, sent: " + responseString);
    }
}
