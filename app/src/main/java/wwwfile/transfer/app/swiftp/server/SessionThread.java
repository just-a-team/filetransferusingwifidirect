package wwwfile.transfer.app.swiftp.server;

import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.ByteBuffer;

import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.swiftp.Defaults;
import wwwfile.transfer.app.swiftp.FsApp;
import wwwfile.transfer.app.swiftp.FsService;
import wwwfile.transfer.app.swiftp.FsSettings;

public class SessionThread extends Thread {
    protected boolean shouldExit = false;
    protected Socket cmdSocket;
    protected ByteBuffer buffer = ByteBuffer.allocate(Defaults.getInputBufferSize());
    protected boolean pasvMode = false;
    protected boolean binaryMode = false;
    protected Account account = new Account();
    protected boolean userAuthenticated = false;
    protected File workingDir = FsSettings.getChrootDir();
    protected Socket dataSocket = null;
    protected File renameFrom = null;
    protected LocalDataSocket localDataSocket;
    OutputStream dataOutputStream = null;
    private boolean sendWelcomeBanner;
    protected String encoding = Defaults.SESSION_ENCODING;
    protected long offset = -1;
    int authFails = 0;

    public static int MAX_AUTH_FAILS = 3;

    public SessionThread(Socket socket, LocalDataSocket dataSocket) {
        this.cmdSocket = socket;
        this.localDataSocket = dataSocket;
        this.sendWelcomeBanner = true;
    }
    public boolean sendViaDataSocket(String string) {
        try {
            byte[] bytes = string.getBytes(encoding);
            Log.d(Constants.TAG, "Using data connection encoding: " + encoding);
            return sendViaDataSocket(bytes, bytes.length);
        } catch (UnsupportedEncodingException e) {
            Log.e(Constants.TAG, "Unsupported encoding for data socket send");
            return false;
        }
    }

    public boolean sendViaDataSocket(byte[] bytes, int len) {
        return sendViaDataSocket(bytes, 0, len);
    }
    public boolean sendViaDataSocket(byte[] bytes, int start, int len) {

        if (dataOutputStream == null) {
            Log.i(Constants.TAG, "Can't send via null dataOutputStream");
            return false;
        }
        if (len == 0) {
            return true;
        }
        try {
            dataOutputStream.write(bytes, start, len);
        } catch (IOException e) {
            Log.i(Constants.TAG, "Couldn't write output stream for data socket");
            Log.i(Constants.TAG, e.toString());
            return false;
        }
        localDataSocket.reportTraffic(len);
        return true;
    }
    public int receiveFromDataSocket(byte[] buf) {
        int bytesRead;

        if (dataSocket == null) {
            Log.i(Constants.TAG, "Can't receive from null dataSocket");
            return -2;
        }
        if (!dataSocket.isConnected()) {
            Log.i(Constants.TAG, "Can't receive from unconnected socket");
            return -2;
        }
        InputStream in;
        try {
            in = dataSocket.getInputStream();
            while ((bytesRead = in.read(buf, 0, buf.length)) == 0) {
            }
            if (bytesRead == -1) {
                return -1;
            }
        } catch (IOException e) {
            Log.i(Constants.TAG, "Error reading data socket");
            return 0;
        }
        localDataSocket.reportTraffic(bytesRead);
        return bytesRead;
    }
    public int onPasv() {
        return localDataSocket.onPasv();
    }
    public boolean onPort(InetAddress dest, int port) {
        return localDataSocket.onPort(dest, port);
    }

    public InetAddress getDataSocketPasvIp() {
        return cmdSocket.getLocalAddress();
    }
    public boolean startUsingDataSocket() {
        try {
            dataSocket = localDataSocket.onTransfer();
            if (dataSocket == null) {
                Log.i(Constants.TAG, "dataSocketFactory.onTransfer() returned null");
                return false;
            }
            dataOutputStream = dataSocket.getOutputStream();
            return true;
        } catch (IOException e) {
            Log.i(Constants.TAG, "IOException getting OutputStream for data socket");
            dataSocket = null;
            return false;
        }
    }

    public void quit() {
        Log.d(Constants.TAG, "SessionThread told to quit");
        closeSocket();
    }

    public void closeDataSocket() {
        Log.d(Constants.TAG, "Closing data socket");
        if (dataOutputStream != null) {
            try {
                dataOutputStream.close();
            } catch (IOException e) {
            }
            dataOutputStream = null;
        }
        if (dataSocket != null) {
            try {
                dataSocket.close();
            } catch (IOException e) {
            }
        }
        dataSocket = null;
    }

    protected InetAddress getLocalAddress() {
        return cmdSocket.getLocalAddress();
    }

    @Override
    public void run() {
        Log.i(Constants.TAG, "SessionThread started");

        if (sendWelcomeBanner) {
            writeString("220 SwiFTP " + FsApp.getVersion() + " ready\r\n");
        }
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    cmdSocket.getInputStream()), 8192);
            while (true) {
                String line;
                line = in.readLine();
                if (line != null) {
                    FsService.writeMonitor(true, line);
                    Log.d(Constants.TAG, "Received line from client: " + line);
                    FtpCmd.dispatchCommand(this, line);
                } else {
                    Log.i(Constants.TAG, "readLine gave null, quitting");
                    break;
                }
            }
        } catch (IOException e) {
            Log.i(Constants.TAG, "Connection was dropped");
        }
        closeSocket();
    }
    public static boolean compareLen(byte[] array1, byte[] array2, int len) {
        for (int i = 0; i < len; i++) {
            if (array1[i] != array2[i]) {
                return false;
            }
        }
        return true;
    }

    public void closeSocket() {
        if (cmdSocket == null) {
            return;
        }
        try {
            cmdSocket.close();
        } catch (IOException e) {
        }
    }

    public void writeBytes(byte[] bytes) {
        try {
            BufferedOutputStream out = new BufferedOutputStream(cmdSocket.getOutputStream(), Defaults.dataChunkSize);
            out.write(bytes);
            out.flush();
            localDataSocket.reportTraffic(bytes.length);
        } catch (IOException e) {
            Log.i(Constants.TAG, "Exception writing socket");
            closeSocket();
            return;
        }
    }

    public void writeString(String str) {
        FsService.writeMonitor(false, str);
        byte[] strBytes;
        try {
            strBytes = str.getBytes(encoding);
        } catch (UnsupportedEncodingException e) {
            Log.e(Constants.TAG, "Unsupported encoding: " + encoding);
            strBytes = str.getBytes();
        }
        writeBytes(strBytes);
    }
    protected Socket getSocket() {
        return cmdSocket;
    }
    public Account getAccount() {
        return account;
    }
    public void setAccount(Account account) {
        this.account = account;
    }
    public boolean isPasvMode() {
        return pasvMode;
    }
    static public ByteBuffer stringToBB(String s) {
        return ByteBuffer.wrap(s.getBytes());
    }
    public boolean isBinaryMode() {
        return binaryMode;
    }
    public void setBinaryMode(boolean binaryMode) {
        this.binaryMode = binaryMode;
    }
    public boolean isAuthenticated() {
        if (userAuthenticated == true || FsSettings.allowAnoymous() == true) {
            return true;
        }
        return false;
    }
    public boolean isAnonymouslyLoggedIn() {
        if (userAuthenticated == true) {
            return false;
        }
        if (FsSettings.allowAnoymous() == true) {
            return true;
        }
        return false;
    }
    public boolean isUserLoggedIn() {
        return userAuthenticated;
    }

    public void authAttempt(boolean authenticated) {
        if (authenticated) {
            Log.i(Constants.TAG, "Authentication complete");
            userAuthenticated = true;
        } else {
            authFails++;
            Log.i(Constants.TAG, "Auth failed: " + authFails + "/" + MAX_AUTH_FAILS);
            if (authFails > MAX_AUTH_FAILS) {
                Log.i(Constants.TAG, "Too many auth fails, quitting session");
                quit();
            }
        }
    }

    public File getWorkingDir() {
        return workingDir;
    }

    public void setWorkingDir(File workingDir) {
        try {
            this.workingDir = workingDir.getCanonicalFile().getAbsoluteFile();
        } catch (IOException e) {
            Log.i(Constants.TAG, "SessionThread canonical error");
        }
    }
    public Socket getDataSocket() {
        return dataSocket;
    }
    public void setDataSocket(Socket dataSocket) {
        this.dataSocket = dataSocket;
    }
    public File getRenameFrom() {
        return renameFrom;
    }
    public void setRenameFrom(File renameFrom) {
        this.renameFrom = renameFrom;
    }
    public String getEncoding() {
        return encoding;
    }
    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }
}
