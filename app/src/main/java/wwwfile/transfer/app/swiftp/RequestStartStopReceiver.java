package wwwfile.transfer.app.swiftp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.filetransferapp.R;

public class RequestStartStopReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v(Constants.TAG, "Received: " + intent.getAction());
        try {
            if (intent.getAction().equals(FsService.ACTION_START_FTPSERVER)) {
                Intent serverService = new Intent(context, FsService.class);
                if (!FsService.isRunning()) {
                    warnIfNoExternalStorage();
                    context.startService(serverService);
                }
            } else if (intent.getAction().equals(FsService.ACTION_STOP_FTPSERVER)) {
                Intent serverService = new Intent(context, FsService.class);
                context.stopService(serverService);
            }
        } catch (Exception e) {
            Log.e(Constants.TAG, "Failed to start/stop on intent " + e.getMessage());
        }
    }
    private void warnIfNoExternalStorage() {
        String storageState = Environment.getExternalStorageState();
        if (!storageState.equals(Environment.MEDIA_MOUNTED)) {
            Log.v(Constants.TAG, "Warning due to storage state " + storageState);
            Toast toast = Toast.makeText(FsApp.getAppContext(), R.string.storage_warning, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }
	
}
