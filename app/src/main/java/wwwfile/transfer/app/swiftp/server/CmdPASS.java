package wwwfile.transfer.app.swiftp.server;

import android.util.Log;

import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.swiftp.FsSettings;
import wwwfile.transfer.app.swiftp.Util;

public class CmdPASS extends FtpCmd implements Runnable {
    String input;

    public CmdPASS(SessionThread sessionThread, String input) {
        super(sessionThread);
        this.input = input;
    }

    @Override
    public void run() {
        Log.d(Constants.TAG, "Executing PASS");
        String attemptPassword = getParameter(input, true);
        String attemptUsername = sessionThread.account.getUsername();
        if (attemptUsername == null) {
            sessionThread.writeString("503 Must send USER first\r\n");
            return;
        }
        String username = FsSettings.getUserName();
        String password = FsSettings.getPassWord();
        if (username == null || password == null) {
            Log.e(Constants.TAG, "Username or password misconfigured");
            sessionThread.writeString("500 Internal error during authentication");
        } else if (username.equals(attemptUsername) && password.equals(attemptPassword)) {
            sessionThread.writeString("230 Access granted\r\n");
            Log.i(Constants.TAG, "User " + username + " password verified");
            sessionThread.authAttempt(true);
        } else if (attemptUsername.equals("anonymous") && FsSettings.allowAnoymous()) {
            sessionThread.writeString("230 Guest login ok, read only access.\r\n");
            Log.i(Constants.TAG, "Guest logged in with email: " + attemptPassword);
        } else {
            Log.i(Constants.TAG, "Failed authentication");
            Util.sleepIgnoreInterupt(1000);
            sessionThread.writeString("530 Login incorrect.\r\n");
            sessionThread.authAttempt(false);
        }
    }
}
