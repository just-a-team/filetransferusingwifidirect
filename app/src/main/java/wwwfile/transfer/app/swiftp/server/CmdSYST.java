package wwwfile.transfer.app.swiftp.server;

import android.util.Log;

import wwwfile.transfer.app.base.Constants;

public class CmdSYST extends FtpCmd implements Runnable {
    public static final String response = "215 UNIX Type: L8\r\n";

    public CmdSYST(SessionThread sessionThread, String input) {
        super(sessionThread);
    }

    @Override
    public void run() {
        Log.d(Constants.TAG, "SYST executing");
        sessionThread.writeString(response);
        Log.d(Constants.TAG, "SYST finished");
    }
}
