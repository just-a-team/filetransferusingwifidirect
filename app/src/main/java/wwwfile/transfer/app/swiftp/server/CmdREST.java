package wwwfile.transfer.app.swiftp.server;

import android.util.Log;

import wwwfile.transfer.app.base.Constants;

public class CmdREST extends FtpCmd implements Runnable {
    protected String input;

    public CmdREST(SessionThread sessionThread, String input) {
        super(sessionThread);
        this.input = input;
    }

    @Override
    public void run() {
        String param = getParameter(input);
        long offset;
        try {
            offset = Long.parseLong(param);
        } catch (NumberFormatException e) {
            sessionThread.writeString("550 No valid restart position\r\n");
            return;
        }
        Log.d(Constants.TAG, "run REST with offset " + offset);
        if (sessionThread.isBinaryMode()) {
            sessionThread.offset = offset;
            sessionThread.writeString("350 Restart position accepted (" + offset
                    + ")\r\n");
        } else {
            if (offset != 0) {
                sessionThread.writeString("550 Restart position > 0 not accepted"
                        + " in ASCII mode\r\n");
            } else {
                sessionThread.offset = offset;
                sessionThread.writeString("350 Restart position accepted (" + offset
                        + ")\r\n");
            }
        }
    }

}