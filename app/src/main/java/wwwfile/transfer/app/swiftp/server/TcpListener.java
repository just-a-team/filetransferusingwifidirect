package wwwfile.transfer.app.swiftp.server;

import android.util.Log;

import java.net.ServerSocket;
import java.net.Socket;

import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.swiftp.FsService;

public class TcpListener extends Thread {
    ServerSocket listenSocket;
    FsService ftpServerService;

    public TcpListener(ServerSocket listenSocket, FsService ftpServerService) {
        this.listenSocket = listenSocket;
        this.ftpServerService = ftpServerService;
    }
    public void quit() {
        try {
            listenSocket.close();
        } catch (Exception e) {
            Log.d(Constants.TAG, "Exception closing TcpListener listenSocket");
        }
    }

    @Override
    public void run() {
        try {
            while (true) {
                Socket clientSocket = listenSocket.accept();
                Log.i(Constants.TAG, "New connection, spawned thread");
                SessionThread newSession = new SessionThread(clientSocket,
                        new LocalDataSocket());
                newSession.start();
                ftpServerService.registerSessionThread(newSession);
            }
        } catch (Exception e) {
            Log.d(Constants.TAG, "Exception in TcpListener");
        }
    }
}
