package wwwfile.transfer.app.swiftp.server;

import android.util.Log;

import wwwfile.transfer.app.base.Constants;

public class CmdUSER extends FtpCmd implements Runnable {
    protected String input;

    public CmdUSER(SessionThread sessionThread, String input) {
        super(sessionThread);
        this.input = input;

    }

    @Override
    public void run() {
        Log.d(Constants.TAG, "USER executing");
        String username = FtpCmd.getParameter(input);
        if (!username.matches("[A-Za-z0-9]+")) {
            sessionThread.writeString("530 Invalid username\r\n");
            return;
        }
        sessionThread.writeString("331 Send password\r\n");
        sessionThread.account.setUsername(username);
    }

}
