package wwwfile.transfer.app.swiftp;

public class Defaults {
	protected static int inputBufferSize = 256;
	public static int dataChunkSize = 65536;
	public static final int tcpConnectionBacklog = 5;
	public static final int SO_TIMEOUT_MS = 30000;
	public static final String SESSION_ENCODING = "UTF-8";

	public static final boolean do_mediascanner_notify = true;

	public static int getInputBufferSize() {
		return inputBufferSize;
	}

	public static void setInputBufferSize(int inputBufferSize) {
		Defaults.inputBufferSize = inputBufferSize;
	}

	public static int getDataChunkSize() {
		return dataChunkSize;
	}

	public static void setDataChunkSize(int dataChunkSize) {
		Defaults.dataChunkSize = dataChunkSize;
	}
}
