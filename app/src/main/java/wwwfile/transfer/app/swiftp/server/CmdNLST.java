package wwwfile.transfer.app.swiftp.server;

import android.util.Log;

import java.io.File;

import wwwfile.transfer.app.base.Constants;

public class CmdNLST extends CmdAbstractListing implements Runnable {
    public final static long MS_IN_SIX_MONTHS = 6L * 30L * 24L * 60L * 60L * 1000L;
    private final String input;

    public CmdNLST(SessionThread sessionThread, String input) {
        super(sessionThread, input);
        this.input = input;
    }

    @Override
    public void run() {
        String errString = null;

        mainblock: {
            String param = getParameter(input);
            if (param.startsWith("-")) {
                param = "";
            }
            File fileToList = null;
            if (param.equals("")) {
                fileToList = sessionThread.getWorkingDir();
            } else {
                if (param.contains("*")) {
                    errString = "550 NLST does not support wildcards\r\n";
                    break mainblock;
                }
                fileToList = new File(sessionThread.getWorkingDir(), param);
                if (violatesChroot(fileToList)) {
                    errString = "450 Listing target violates chroot\r\n";
                    break mainblock;
                } else if (fileToList.isFile()) {
                    errString = "550 NLST for regular files is unsupported\r\n";
                    break mainblock;
                }
            }
            String listing;
            if (fileToList.isDirectory()) {
                StringBuilder response = new StringBuilder();
                errString = listDirectory(response, fileToList);
                if (errString != null) {
                    break mainblock;
                }
                listing = response.toString();
            } else {
                listing = makeLsString(fileToList);
                if (listing == null) {
                    errString = "450 Couldn't list that file\r\n";
                    break mainblock;
                }
            }
            errString = sendListing(listing);
            if (errString != null) {
                break mainblock;
            }
        }

        if (errString != null) {
            sessionThread.writeString(errString);
            Log.d(Constants.TAG, "NLST failed with: " + errString);
        } else {
            Log.d(Constants.TAG, "NLST completed OK");
        }
    }

    @Override
    protected String makeLsString(File file) {
        if (!file.exists()) {
            Log.i(Constants.TAG, "makeLsString had nonexistent file");
            return null;
        }
        String lastNamePart = file.getName();
        if (lastNamePart.contains("*") || lastNamePart.contains("/")) {
            Log.i(Constants.TAG, "Filename omitted due to disallowed character");
            return null;
        } else {
            Log.d(Constants.TAG, "Filename: " + lastNamePart);
            return lastNamePart + "\r\n";
        }
    }
}
