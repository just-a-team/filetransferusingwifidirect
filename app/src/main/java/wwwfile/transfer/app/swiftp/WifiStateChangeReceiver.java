package wwwfile.transfer.app.swiftp;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import wwwfile.transfer.app.base.Constants;

public class WifiStateChangeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
        if (info == null) {
            Log.e(Constants.TAG,"Null network info received, bailing");
            return;
        }
        if (info.isConnected()) {
            Log.d(Constants.TAG,"We are connecting to a wifi network");
            Intent startServerIntent = new Intent(context, StartServerService.class);
            context.startService(startServerIntent);
        } else {
            Log.d(Constants.TAG,"We are disconnected from wifi network");
            Intent stopServerIntent = new Intent(context, StopServerService.class);
            context.startService(stopServerIntent);
        }
    }

    static public class StartServerService extends IntentService {

        public StartServerService() {
            super(StartServerService.class.getSimpleName());
        }

        @Override
        protected void onHandleIntent(Intent intent) {
            if (FsService.isRunning()) {
                Log.v(Constants.TAG,"We are connecting to a new wifi network on a running server, ignore");
                return;
            }
            WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            if (wifiInfo == null) {
                Log.e(Constants.TAG,"Null wifi info received, bailing");
                return;
            }
            Log.d(Constants.TAG,"We are connected to " + wifiInfo.getSSID());
            if (FsSettings.getAutoConnectList().contains(wifiInfo.getSSID())) {
                Util.sleepIgnoreInterupt(1000);
                sendBroadcast(new Intent(FsService.ACTION_START_FTPSERVER));
            }
        }
    }

    static public class StopServerService extends IntentService {

        public StopServerService() {
            super(StopServerService.class.getSimpleName());
        }

        @Override
        protected void onHandleIntent(Intent intent) {
            if (!FsService.isRunning()) return;

            Util.sleepIgnoreInterupt(15000);
            if (!FsService.isRunning()) return;

            ConnectivityManager conManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = conManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (netInfo.isConnectedOrConnecting()) return;

            Log.d(Constants.TAG,"Wifi connection disconnected and no longer connecting, stopping the ftp server");
            sendBroadcast(new Intent(FsService.ACTION_STOP_FTPSERVER));
        }
    }
}