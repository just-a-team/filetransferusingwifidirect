package wwwfile.transfer.app.swiftp.server;

import android.util.Log;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.swiftp.Util;

public class CmdMFMT extends FtpCmd implements Runnable {
    private String mInput;

    public CmdMFMT(SessionThread sessionThread, String input) {
        super(sessionThread);
        mInput = input;
    }

    @Override
    public void run() {
        Log.d(Constants.TAG, "run: MFMT executing, input: " + mInput);
        String[] params = getParameter(mInput).split(" ");

        if (params.length != 2) {
            sessionThread.writeString("500 wrong number of parameters\r\n");
            Log.d(Constants.TAG, "run: MFMT failed, wrong number of parameters");
            return;
        }
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddhhmmss", Locale.US);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date timeVal;
        try {
            timeVal = Util.parseDate(params[0]);
        } catch (ParseException e) {
            sessionThread.writeString("501 unable to parse parameter time-val\r\n");
            Log.d(Constants.TAG, "run: MFMT failed, unable to parse parameter time-val");
            return;
        }

        String pathName = params[1];
        File file = inputPathToChrootedFile(sessionThread.getWorkingDir(), pathName);

        if (file.exists() == false) {
            sessionThread.writeString("550 file does not exist on server\r\n");
            Log.d(Constants.TAG, "run: MFMT failed, file does not exist");
            return;
        }

        boolean success = file.setLastModified(timeVal.getTime());
        if (success == false) {
            sessionThread.writeString("500 unable to modify last modification time\r\n");
            Log.d(Constants.TAG, "run: MFMT failed, unable to modify last modification time");
            return;
        }

        long lastModified = file.lastModified();
        String response = "213 " + df.format(new Date(lastModified)) + "; "
                + file.getAbsolutePath() + "\r\n";
        sessionThread.writeString(response);
        Log.d(Constants.TAG, "run: MFMT completed succesful");
    }
}