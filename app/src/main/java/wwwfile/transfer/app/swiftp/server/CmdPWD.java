package wwwfile.transfer.app.swiftp.server;

import android.util.Log;

import java.io.IOException;

import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.swiftp.FsSettings;

public class CmdPWD extends FtpCmd implements Runnable {
    public CmdPWD(SessionThread sessionThread, String input) {
        super(sessionThread);
    }

    @Override
    public void run() {
        Log.d(Constants.TAG, "PWD executing");
        try {
            String currentDir = sessionThread.getWorkingDir().getCanonicalPath();
            currentDir = currentDir.substring(FsSettings.getChrootDir().getCanonicalPath()
                    .length());
            if (currentDir.length() == 0) {
                currentDir = "/";
            }
            sessionThread.writeString("257 \"" + currentDir + "\"\r\n");
        } catch (IOException e) {
            Log.e(Constants.TAG, "PWD canonicalize");
            sessionThread.closeSocket();
        }
        Log.d(Constants.TAG, "PWD complete");
    }

}
