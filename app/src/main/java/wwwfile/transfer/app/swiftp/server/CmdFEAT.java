package wwwfile.transfer.app.swiftp.server;

import android.util.Log;

import wwwfile.transfer.app.base.Constants;

public class CmdFEAT extends FtpCmd implements Runnable {
    public CmdFEAT(SessionThread sessionThread, String input) {
        super(sessionThread);
    }

    @Override
    public void run() {
        Log.d(Constants.TAG, "run: Giving FEAT");
        sessionThread.writeString("211-Features supported by FTP Server\r\n");
        sessionThread.writeString(" UTF8\r\n");
        sessionThread.writeString(" MDTM\r\n");
        sessionThread.writeString(" MFMT\r\n");
        sessionThread.writeString("211 End\r\n");
        Log.d(Constants.TAG, "run: Gave FEAT");
    }

}
