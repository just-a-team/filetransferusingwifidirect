package wwwfile.transfer.app.swiftp.gui;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;

import java.net.InetAddress;

import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.filetransferapp.R;
import wwwfile.transfer.app.swiftp.FsService;

public class FsWidgetProvider extends AppWidgetProvider {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v(Constants.TAG, "Received broadcast: " + intent.getAction());
        final String action = intent.getAction();
        if (action.equals(FsService.ACTION_STARTED)
                || action.equals(FsService.ACTION_STOPPED)) {
            Intent updateIntent = new Intent(context, UpdateService.class);
            context.startService(updateIntent);
        }
        super.onReceive(context, intent);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        Log.d(Constants.TAG, "updated called");
        Intent intent = new Intent(context, UpdateService.class);
        context.startService(intent);
    }

    public static class UpdateService extends Service {
        @Override
        public int onStartCommand(Intent intent, int flags, int startId) {
            Log.d(Constants.TAG, "UpdateService start command");
            final String action;
            final int drawable;
            final String text;
            if (FsService.isRunning()) {
                action = FsService.ACTION_STOP_FTPSERVER;
                drawable = R.drawable.widget_on;
                InetAddress address = FsService.getLocalInetAddress();
                if (address == null) {
                    Log.w(Constants.TAG,"Unable to retrieve the local ip address");
                    text = "ERROR";
                } else {
                    text = address.getHostAddress();
                }
            } else {
                action = FsService.ACTION_START_FTPSERVER;
                drawable = R.drawable.widget_off;
                text = getString(R.string.app_name);
            }
            Intent startIntent = new Intent(action);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0,
                    startIntent, 0);
            RemoteViews views = new RemoteViews(getPackageName(), R.layout.widget_layout);
            views.setOnClickPendingIntent(R.id.widget_button, pendingIntent);
            views.setImageViewResource(R.id.widget_button, drawable);
            views.setTextViewText(R.id.widget_text, text);
            AppWidgetManager manager = AppWidgetManager.getInstance(this);
            ComponentName widget = new ComponentName(this, FsWidgetProvider.class);
            manager.updateAppWidget(widget, views);
            return START_NOT_STICKY;
        }

        @Override
        public IBinder onBind(Intent intent) {
            return null;
        }
    }
}
