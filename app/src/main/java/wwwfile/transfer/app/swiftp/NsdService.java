package wwwfile.transfer.app.swiftp;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdManager.RegistrationListener;
import android.net.nsd.NsdServiceInfo;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.filetransferapp.R;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class NsdService extends Service {
    private static final String FTP_SERVICE_TYPE= "_ftp._tcp.";
    private NsdManager mNsdManager = null;
    private volatile boolean running = false;
    public static class StartStopReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(Constants.TAG, "onReceive broadcast: " + intent.getAction());
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                Log.w(Constants.TAG, "onReceive: Running pre-JB, version to old for "
                        + "NSD functionality, bailing out");
                return;
            }
            if (intent.getAction().equals(FsService.ACTION_STARTED)) {
                Intent service = new Intent(context, NsdService.class);
                context.startService(service);
            } else if (intent.getAction().equals(FsService.ACTION_STOPPED)) {
                Intent service = new Intent(context, NsdService.class);
                context.stopService(service);
            }
        }

    }
    private RegistrationListener mRegistrationListener = new RegistrationListener() {
        @Override
        public void onServiceRegistered(NsdServiceInfo serviceInfo) {
            Log.d(Constants.TAG, "onServiceRegistered: " + serviceInfo.getServiceName());
        }
        @Override
        public void onServiceUnregistered(NsdServiceInfo serviceInfo) {
            Log.d(Constants.TAG, "onServiceUnregistered: " + serviceInfo.getServiceName());
        }
        @Override
        public void onRegistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
            Log.e(Constants.TAG, "onRegistrationFailed: errorCode=" + errorCode);
        }

        @Override
        public void onUnregistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
            Log.e(Constants.TAG, "onUnregistrationFailed: errorCode=" + errorCode);
        }
    };

    @Override
    public void onCreate() {
        Log.d(Constants.TAG, "onCreate: Entered");

        running = true;

        Resources res = getResources();
        String serviceNamePostfix = res.getString(R.string.nsd_servername_postfix);
        String serviceName = Build.MODEL + " " + serviceNamePostfix;

        final NsdServiceInfo serviceInfo = new NsdServiceInfo();
        serviceInfo.setServiceName(serviceName);
        serviceInfo.setServiceType(FTP_SERVICE_TYPE);
        serviceInfo.setPort(FsSettings.getPortNumber());

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(Constants.TAG, "onCreate: Trying to get the NsdManager");
                mNsdManager = (NsdManager) getSystemService(Context.NSD_SERVICE);
                if (mNsdManager != null) {
                    Log.d(Constants.TAG, "onCreate: Got the NsdManager");
                    try {
                        Thread.sleep(500);
                        if (running == false) {
                            Log.e(Constants.TAG, "NsdManager is no longer needed, bailing out");
                            mNsdManager = null;
                            return;
                        }
                        mNsdManager.registerService(serviceInfo,
                                NsdManager.PROTOCOL_DNS_SD, mRegistrationListener);
                    } catch (Exception e) {
                        Log.e(Constants.TAG, "onCreate: Failed to register NsdManager");
                        mNsdManager = null;
                    }
                } else {
                    Log.d(Constants.TAG, "onCreate: Failed to get the NsdManager");
                }
            }
        }).start();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(Constants.TAG, "onStartCommand: Entered");

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d(Constants.TAG, "onDestroy: Entered");

        running = false;

        if (mNsdManager == null) {
            Log.e(Constants.TAG, "unregisterService: Unexpected mNsdManger to be null, bailing out");
            return;
        }
        try {
            mNsdManager.unregisterService(mRegistrationListener);
        } catch (Exception e) {
            Log.e(Constants.TAG, "Unable to unregister NSD service, error: " + e.getMessage());
        }
        mNsdManager = null;
    }
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
