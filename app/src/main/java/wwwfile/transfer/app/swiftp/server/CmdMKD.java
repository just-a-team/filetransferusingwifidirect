package wwwfile.transfer.app.swiftp.server;

import android.util.Log;

import java.io.File;

import wwwfile.transfer.app.base.Constants;

public class CmdMKD extends FtpCmd implements Runnable {
    String input;

    public CmdMKD(SessionThread sessionThread, String input) {
        super(sessionThread);
        this.input = input;
    }

    @Override
    public void run() {
        Log.d(Constants.TAG, "MKD executing");
        String param = getParameter(input);
        File toCreate;
        String errString = null;
        mainblock: {
            if (param.length() < 1) {
                errString = "550 Invalid name\r\n";
                break mainblock;
            }
            toCreate = inputPathToChrootedFile(sessionThread.getWorkingDir(), param);
            if (violatesChroot(toCreate)) {
                errString = "550 Invalid name or chroot violation\r\n";
                break mainblock;
            }
            if (toCreate.exists()) {
                errString = "550 Already exists\r\n";
                break mainblock;
            }
            if (!toCreate.mkdir()) {
                errString = "550 Error making directory (permissions?)\r\n";
                break mainblock;
            }
        }
        if (errString != null) {
            sessionThread.writeString(errString);
            Log.i(Constants.TAG, "MKD error: " + errString.trim());
        } else {
            sessionThread.writeString("250 Directory created\r\n");
        }
        Log.i(Constants.TAG, "MKD complete");
    }

}
