package wwwfile.transfer.app.swiftp.server;

import android.util.Log;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;

import wwwfile.transfer.app.base.Constants;

public abstract class CmdAbstractListing extends FtpCmd {

    public CmdAbstractListing(SessionThread sessionThread, String input) {
        super(sessionThread);
    }

    abstract String makeLsString(File file);

    public String listDirectory(StringBuilder response, File dir) {
        if (!dir.isDirectory()) {
            return "500 Internal error, listDirectory on non-directory\r\n";
        }
        Log.d(Constants.TAG, "Listing directory: " + dir.toString());
        File[] entries = dir.listFiles();
        if (entries == null) {
            return "500 Couldn't list directory. Check config and mount status.\r\n";
        }
        Log.d(Constants.TAG, "Dir len " + entries.length);
        try {

            Arrays.sort(entries, listingComparator);
        } catch (Exception e) {
            Log.e(Constants.TAG, "Unable to sort the listing: " + e.getMessage());
            entries = dir.listFiles();
        }
        for (File entry : entries) {
            String curLine = makeLsString(entry);
            if (curLine != null) {
                response.append(curLine);
            }
        }
        return null;
    }
    protected String sendListing(String listing) {
        if (sessionThread.startUsingDataSocket()) {
            Log.d(Constants.TAG, "LIST/NLST done making socket");
        } else {
            sessionThread.closeDataSocket();
            return "425 Error opening data socket\r\n";
        }
        String mode = sessionThread.isBinaryMode() ? "BINARY" : "ASCII";
        sessionThread.writeString("150 Opening " + mode
                + " mode data connection for file list\r\n");
        Log.d(Constants.TAG, "Sent code 150, sending listing string now");
        if (!sessionThread.sendViaDataSocket(listing)) {
            Log.d(Constants.TAG, "sendViaDataSocket failure");
            sessionThread.closeDataSocket();
            return "426 Data socket or network error\r\n";
        }
        sessionThread.closeDataSocket();
        Log.d(Constants.TAG, "Listing sendViaDataSocket success");
        sessionThread.writeString("226 Data transmission OK\r\n");
        return null;
    }

    static final Comparator<File> listingComparator =new Comparator<File>() {
        @Override
        public int compare(File lhs, File rhs) {
            if (lhs.isDirectory() && rhs.isFile()) {
                return -1;
            } else if (lhs.isFile() && rhs.isDirectory()) {
                return 1;
            } else {
                return lhs.getName().compareToIgnoreCase(rhs.getName());
            }
        }
    };
}
