package wwwfile.transfer.app.swiftp.server;

import android.util.Log;

import wwwfile.transfer.app.base.Constants;

public class CmdTemplate extends FtpCmd implements Runnable {
    public static final String message = "TEMPLATE!!";

    public CmdTemplate(SessionThread sessionThread, String input) {
        super(sessionThread);
    }

    @Override
    public void run() {
        sessionThread.writeString(message);
        Log.i(Constants.TAG, "Template log message");
    }

}
