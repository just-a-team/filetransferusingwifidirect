package wwwfile.transfer.app.swiftp.server;

import android.util.Log;

import wwwfile.transfer.app.base.Constants;

public class CmdOPTS extends FtpCmd implements Runnable {
    public static final String message = "TEMPLATE!!";
    private final String input;

    public CmdOPTS(SessionThread sessionThread, String input) {
        super(sessionThread);
        this.input = input;
    }

    @Override
    public void run() {
        String param = getParameter(input);
        String errString = null;

        mainBlock: {
            if (param == null) {
                errString = "550 Need argument to OPTS\r\n";
                Log.w(Constants.TAG, "Couldn't understand empty OPTS command");
                break mainBlock;
            }
            String[] splits = param.split(" ");
            if (splits.length != 2) {
                errString = "550 Malformed OPTS command\r\n";
                Log.w(Constants.TAG, "Couldn't parse OPTS command");
                break mainBlock;
            }
            String optName = splits[0].toUpperCase();
            String optVal = splits[1].toUpperCase();
            if (optName.equals("UTF8")) {
                if (optVal.equals("ON")) {
                    Log.d(Constants.TAG, "Got OPTS UTF8 ON");
                    sessionThread.setEncoding("UTF-8");
                } else {
                    Log.i(Constants.TAG, "Ignoring OPTS UTF8 for something besides ON");
                }
                break mainBlock;
            } else {
                Log.d(Constants.TAG, "Unrecognized OPTS option: " + optName);
                errString = "502 Unrecognized option\r\n";
                break mainBlock;
            }
        }
        if (errString != null) {
            sessionThread.writeString(errString);
        } else {
            sessionThread.writeString("200 OPTS accepted\r\n");
            Log.d(Constants.TAG, "Handled OPTS ok");
        }
    }

}
