package wwwfile.transfer.app.swiftp;

import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Environment;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

import wwwfile.transfer.app.base.Constants;

public enum MediaUpdater {
    INSTANCE;
    private static Timer sTimer = new Timer();
    private static class ScanCompletedListener implements
            MediaScannerConnection.OnScanCompletedListener {
        @Override
        public void onScanCompleted(String path, Uri uri) {
            Log.i(Constants.TAG, "Scan completed: " + path + " : " + uri);
        }
    }
    public static void notifyFileCreated(String path) {
        if (Defaults.do_mediascanner_notify) {
            Log.d(Constants.TAG, "Notifying others about new file: " + path);
            Context context = FsApp.getAppContext();
            MediaScannerConnection.scanFile(context, new String[] { path }, null,
                    new ScanCompletedListener());
        }
    }
    public static void notifyFileDeleted(String path) {
        if (Defaults.do_mediascanner_notify) {
            Log.d(Constants.TAG, "Notifying others about deleted file: " + path);
            if (VERSION.SDK_INT < VERSION_CODES.KITKAT) {
                sTimer.cancel();
                sTimer = new Timer();
                sTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        Log.d(Constants.TAG, "Sending ACTION_MEDIA_MOUNTED broadcast");
                        final Context context = FsApp.getAppContext();
                        Uri uri = Uri.parse("file://" + Environment.getExternalStorageDirectory());
                        Intent intent = new Intent(Intent.ACTION_MEDIA_MOUNTED, uri);
                        context.sendBroadcast(intent);
                    }
                }, 5000);
            } else {
                Context context = FsApp.getAppContext();
                MediaScannerConnection.scanFile(context, new String[] { path }, null,
                        new ScanCompletedListener());
            }
        }
    }
}
