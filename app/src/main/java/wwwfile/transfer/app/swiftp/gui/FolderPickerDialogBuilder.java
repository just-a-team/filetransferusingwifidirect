package wwwfile.transfer.app.swiftp.gui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

import wwwfile.transfer.app.base.Constants;

public class FolderPickerDialogBuilder extends AlertDialog.Builder {
    private ArrayAdapter<String> mAdapter;
    private AlertDialog mAlert;
    private File mRoot;
    public FolderPickerDialogBuilder(Context context, File root) {
        super(context);
        mRoot = root;
        mAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1);
        update();
        ListView list = new ListView(getContext());
        list.setAdapter(mAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String dir = (String) parent.getItemAtPosition(position);
                final File file;
                if (dir.equals("..") && (file = mRoot.getParentFile()) != null) {
                    mRoot = file;
                } else {
                    mRoot = new File(mRoot, dir);
                }
                update();
            }
        });
        setView(list);
    }
    @Override
    public AlertDialog create() {
        if (mAlert != null) throw new RuntimeException("Cannot reuse builder");
        mAlert = super.create();
        return mAlert;
    }
    private void update() {
        try {
            mRoot = new File(mRoot.getCanonicalPath());
        } catch (IOException e) {
            Log.w(Constants.TAG,"Directory root is incorrect, fixing to external storage.");
            mRoot = Environment.getExternalStorageDirectory();
        }
        if (mAlert != null) {
            mAlert.setTitle(mRoot.getAbsolutePath());
        } else {
            setTitle(mRoot.getAbsolutePath());
        }
        mAdapter.clear();
        String[] dirs=mRoot.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                File file = new File(dir, filename);
                return (file.isDirectory() && !file.isHidden());
            }
        });
        if (dirs == null) {
            Log.w(Constants.TAG,"Unable to receive dirs list, no Access rights?");
            Log.d(Constants.TAG,"Unable to fix, continue with empty list");
            dirs = new String[]{};
        }
        mAdapter.add("..");
        mAdapter.addAll(dirs);
    }
    public AlertDialog.Builder setSelectedButton(int textId, final OnSelectedListener listener) {
        return setPositiveButton(textId, new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.onSelected(mRoot.getAbsolutePath());
            }
        });
    }
    public interface OnSelectedListener {
        void onSelected(String path);
    }
}
