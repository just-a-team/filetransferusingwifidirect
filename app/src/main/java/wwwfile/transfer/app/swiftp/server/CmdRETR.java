package wwwfile.transfer.app.swiftp.server;

import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.swiftp.Defaults;

public class CmdRETR extends FtpCmd implements Runnable {
    protected String input;

    public CmdRETR(SessionThread sessionThread, String input) {
        super(sessionThread);
        this.input = input;
    }

    @Override
    public void run() {
        Log.d(Constants.TAG, "RETR executing");
        String param = getParameter(input);
        File fileToRetr;
        String errString = null;

        mainblock: {
            fileToRetr = inputPathToChrootedFile(sessionThread.getWorkingDir(), param);
            if (violatesChroot(fileToRetr)) {
                errString = "550 Invalid name or chroot violation\r\n";
                break mainblock;
            } else if (fileToRetr.isDirectory()) {
                Log.d(Constants.TAG, "Ignoring RETR for directory");
                errString = "550 Can't RETR a directory\r\n";
                break mainblock;
            } else if (!fileToRetr.exists()) {
                Log.d(Constants.TAG, "Can't RETR nonexistent file: " + fileToRetr.getAbsolutePath());
                errString = "550 File does not exist\r\n";
                break mainblock;
            } else if (!fileToRetr.canRead()) {
                Log.i(Constants.TAG, "Failed RETR permission (canRead() is false)");
                errString = "550 No read permissions\r\n";
                break mainblock;
            }
            FileInputStream in = null;
            try {
                in = new FileInputStream(fileToRetr);
                byte[] buffer = new byte[Defaults.getDataChunkSize()];
                int bytesRead;
                if (sessionThread.startUsingDataSocket()) {
                    Log.d(Constants.TAG, "RETR opened data socket");
                } else {
                    errString = "425 Error opening socket\r\n";
                    Log.i(Constants.TAG, "Error in initDataSocket()");
                    break mainblock;
                }
                sessionThread.writeString("150 Sending file\r\n");
                if (sessionThread.isBinaryMode()) {
                    Log.d(Constants.TAG, "Transferring in binary mode");
                    if (sessionThread.offset >= 0) {
                        in.skip(sessionThread.offset);
                    }
                    while ((bytesRead = in.read(buffer)) != -1) {
                        if (sessionThread.sendViaDataSocket(buffer, bytesRead) == false) {
                            errString = "426 Data socket error\r\n";
                            Log.i(Constants.TAG, "Data socket error");
                            break mainblock;
                        }
                    }
                } else {
                    Log.d(Constants.TAG, "Transferring in ASCII mode");
                    if (sessionThread.offset >= 0) {
                        in.skip(sessionThread.offset);
                    }
                    boolean lastBufEndedWithCR = false;
                    while ((bytesRead = in.read(buffer)) != -1) {
                        int startPos = 0, endPos = 0;
                        byte[] crnBuf = { '\r', '\n' };
                        for (endPos = 0; endPos < bytesRead; endPos++) {
                            if (buffer[endPos] == '\n') {
                                sessionThread.sendViaDataSocket(buffer, startPos, endPos
                                        - startPos);
                                if (endPos == 0) {
                                    if (!lastBufEndedWithCR) {
                                        sessionThread.sendViaDataSocket(crnBuf, 1);
                                    }
                                } else if (buffer[endPos - 1] != '\r') {
                                    sessionThread.sendViaDataSocket(crnBuf, 1);
                                } else {
                                }
                                startPos = endPos;
                            }
                        }
                        sessionThread.sendViaDataSocket(buffer, startPos, endPos
                                - startPos);
                        if (buffer[bytesRead - 1] == '\r') {
                            lastBufEndedWithCR = true;
                        } else {
                            lastBufEndedWithCR = false;
                        }
                    }
                }
            } catch (FileNotFoundException e) {
                errString = "550 File not found\r\n";
                break mainblock;
            } catch (IOException e) {
                errString = "425 Network error\r\n";
                break mainblock;
            } finally {
                try {
                    if (in != null)
                        in.close();
                } catch (IOException swallow) {
                }
            }
        }
        sessionThread.closeDataSocket();
        if (errString != null) {
            sessionThread.writeString(errString);
        } else {
            sessionThread.writeString("226 Transmission finished\r\n");
        }
        Log.d(Constants.TAG, "RETR done");
    }
}
