package wwwfile.transfer.app.swiftp.server;

import android.util.Log;

import java.io.File;

import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.swiftp.Util;

public class CmdMDTM extends FtpCmd implements Runnable {
    private String mInput;

    public CmdMDTM(SessionThread sessionThread, String input) {
        super(sessionThread);
        mInput = input;
    }

    @Override
    public void run() {
        Log.d(Constants.TAG, "run: MDTM executing, input: " + mInput);
        String param = getParameter(mInput);
        File file = inputPathToChrootedFile(sessionThread.getWorkingDir(), param);

        if (file.exists()) {
            long lastModified = file.lastModified();
            String response = "213 " + Util.getFtpDate(lastModified) + "\r\n";
            sessionThread.writeString(response);
        } else {
            Log.w(Constants.TAG, "run: file does not exist");
            sessionThread.writeString("550 file does not exist\r\n");
        }

        Log.d(Constants.TAG, "run: MDTM completed");
    }
}