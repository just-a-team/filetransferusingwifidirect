package wwwfile.transfer.app.swiftp.server;

import android.util.Log;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import wwwfile.transfer.app.base.Constants;

public class CmdLIST extends CmdAbstractListing implements Runnable {
    public final static long MS_IN_SIX_MONTHS = 6L * 30L * 24L * 60L * 60L * 1000L;
    private final String input;

    public CmdLIST(SessionThread sessionThread, String input) {
        super(sessionThread, input);
        this.input = input;
    }

    @Override
    public void run() {
        String errString = null;

        mainblock: {
            String param = getParameter(input);
            Log.d(Constants.TAG, "LIST parameter: " + param);
            while (param.startsWith("-")) {
                Log.d(Constants.TAG, "LIST is skipping dashed arg " + param);
                param = getParameter(param);
            }
            File fileToList = null;
            if (param.equals("")) {
                fileToList = sessionThread.getWorkingDir();
            } else {
                if (param.contains("*")) {
                    errString = "550 LIST does not support wildcards\r\n";
                    break mainblock;
                }
                fileToList = new File(sessionThread.getWorkingDir(), param);
                if (violatesChroot(fileToList)) {
                    errString = "450 Listing target violates chroot\r\n";
                    break mainblock;
                }
            }
            String listing;
            if (fileToList.isDirectory()) {
                StringBuilder response = new StringBuilder();
                errString = listDirectory(response, fileToList);
                if (errString != null) {
                    break mainblock;
                }
                listing = response.toString();
            } else {
                listing = makeLsString(fileToList);
                if (listing == null) {
                    errString = "450 Couldn't list that file\r\n";
                    break mainblock;
                }
            }
            errString = sendListing(listing);
            if (errString != null) {
                break mainblock;
            }
        }

        if (errString != null) {
            sessionThread.writeString(errString);
            Log.d(Constants.TAG, "LIST failed with: " + errString);
        } else {
            Log.d(Constants.TAG, "LIST completed OK");
        }
    }
    @Override
    protected String makeLsString(File file) {
        StringBuilder response = new StringBuilder();

        if (!file.exists()) {
            Log.i(Constants.TAG, "makeLsString had nonexistent file");
            return null;
        }
        String lastNamePart = file.getName();
        if (lastNamePart.contains("*") || lastNamePart.contains("/")) {
            Log.i(Constants.TAG, "Filename omitted due to disallowed character");
            return null;
        } else {
        }

        if (file.isDirectory()) {
            response.append("drwxr-xr-x 1 owner group");
        } else {
            response.append("-rw-r--r-- 1 owner group");
        }
        long fileSize = file.length();
        String sizeString = Long.toString(fileSize);
        int padSpaces = 13 - sizeString.length();
        while (padSpaces-- > 0) {
            response.append(' ');
        }
        response.append(sizeString);
        long mTime = file.lastModified();
        SimpleDateFormat format;
        if ((System.currentTimeMillis() - mTime) < MS_IN_SIX_MONTHS) {
            format = new SimpleDateFormat(" MMM dd HH:mm ", Locale.US);
        } else {
            format = new SimpleDateFormat(" MMM dd  yyyy ", Locale.US);
        }
        response.append(format.format(new Date(file.lastModified())));
        response.append(lastNamePart);
        response.append("\r\n");
        return response.toString();
    }

}
