package wwwfile.transfer.app.swiftp.server;

public class CmdSITE extends FtpCmd implements Runnable {

    public CmdSITE(SessionThread sessionThread, String input) {
        super(sessionThread);
    }

    @Override
    public void run() {
        sessionThread.writeString("250 SITE is a NOP\r\n");
    }

}
