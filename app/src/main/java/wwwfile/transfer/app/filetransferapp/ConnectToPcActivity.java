package wwwfile.transfer.app.filetransferapp;


import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.preference.TwoStatePreference;
import android.util.Log;
import android.widget.BaseAdapter;
import android.widget.Toast;

import java.io.File;
import java.net.InetAddress;
import java.util.List;

import wwwfile.transfer.app.android.DynamicMultiSelectListPreference;
import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.swiftp.FsApp;
import wwwfile.transfer.app.swiftp.FsService;
import wwwfile.transfer.app.swiftp.FsSettings;
import wwwfile.transfer.app.swiftp.gui.FolderPickerDialogBuilder;


public class ConnectToPcActivity extends AppCompatPreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private EditTextPreference mPassWordPref;
    private Handler mHandler = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(Constants.TAG, "created");
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        Resources resources = getResources();
        TwoStatePreference runningPref = findPref("running_switch");
        updateRunningState();
        runningPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if ((Boolean) newValue) {
                    startServer();
                } else {
                    stopServer();
                }
                return true;
            }
        });
        PreferenceScreen prefScreen = findPref("preference_screen");
        Preference marketVersionPref = findPref("market_version");
        marketVersionPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setData(Uri.parse("market://details?id=be.ppareit.swiftp"));
                try {
                    startActivity(intent);
                } catch (Exception e) {
                    Log.e(Constants.TAG, "Failed to launch the market.");
                    e.printStackTrace();
                }
                return false;
            }
        });
        if (FsApp.isFreeVersion() == false) {
            prefScreen.removePreference(marketVersionPref);
        }

        updateLoginInfo();

        EditTextPreference usernamePref = findPref("username");
        usernamePref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String newUsername = (String) newValue;
                if (preference.getSummary().equals(newUsername))
                    return false;
                if (!newUsername.matches("[a-zA-Z0-9]+")) {
                    Toast.makeText(ConnectToPcActivity.this,
                            R.string.username_validation_error, Toast.LENGTH_LONG).show();
                    return false;
                }
                stopServer();
                return true;
            }
        });

        mPassWordPref = findPref("password");
        mPassWordPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                stopServer();
                return true;
            }
        });

        DynamicMultiSelectListPreference autoconnectListPref = findPref("autoconnect_preference");
        autoconnectListPref.setOnPopulateListener(new DynamicMultiSelectListPreference.OnPopulateListener() {
            @Override
            public void onPopulate(DynamicMultiSelectListPreference preference) {
                Log.d(Constants.TAG, "autoconnect populate listener");

                WifiManager wifiManager = (WifiManager) ConnectToPcActivity.this.getSystemService(Context.WIFI_SERVICE);
                List<WifiConfiguration> configs = wifiManager.getConfiguredNetworks();
                if (configs == null) {
                    Log.e(Constants.TAG, "Unable to receive wifi configurations, bark at user and bail");
                    Toast.makeText(ConnectToPcActivity.this, R.string.autoconnect_error_enable_wifi_for_access_points, Toast.LENGTH_LONG)
                            .show();
                    return;
                }
                CharSequence[] ssids = new CharSequence[configs.size()];
                CharSequence[] niceSsids = new CharSequence[configs.size()];
                for (int i = 0; i < configs.size(); ++i) {
                    ssids[i] = configs.get(i).SSID;
                    String ssid = configs.get(i).SSID;
                    if (ssid.length() > 2 && ssid.startsWith("\"") && ssid.endsWith("\"")) {
                        ssid = ssid.substring(1, ssid.length() - 1);
                    }
                    niceSsids[i] = ssid;
                }
                preference.setEntries(niceSsids);
                preference.setEntryValues(ssids);
            }
        });

        EditTextPreference portnum_pref = findPref("portNum");
        portnum_pref.setSummary(sp.getString("portNum",
                resources.getString(R.string.portnumber_default)));
        portnum_pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String newPortnumString = (String) newValue;
                if (preference.getSummary().equals(newPortnumString))
                    return false;
                int portnum = 0;
                try {
                    portnum = Integer.parseInt(newPortnumString);
                } catch (Exception e) {
                }
                if (portnum <= 0 || 65535 < portnum) {
                    Toast.makeText(ConnectToPcActivity.this,
                            R.string.port_validation_error, Toast.LENGTH_LONG).show();
                    return false;
                }
                preference.setSummary(newPortnumString);
                stopServer();
                return true;
            }
        });
        Preference chroot_pref = findPref("chrootDir");
        chroot_pref.setSummary(FsSettings.getChrootDirAsString());
        chroot_pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(final Preference preference) {
                AlertDialog folderPicker = new FolderPickerDialogBuilder(ConnectToPcActivity.this, FsSettings.getChrootDir())
                        .setSelectedButton(R.string.select, new FolderPickerDialogBuilder.OnSelectedListener() {
                            @Override
                            public void onSelected(String path) {
                                if (preference.getSummary().equals(path))
                                    return;
                                if (!FsSettings.setChrootDir(path))
                                    return;
                                // TODO: this is a hotfix, create correct resources, improve UI/UX
                                final File root = new File(path);
                                if (!root.canRead()) {
                                    Toast.makeText(ConnectToPcActivity.this,
                                            "Notice that we can't read/write in this folder.",
                                            Toast.LENGTH_LONG).show();
                                } else if (!root.canWrite()) {
                                    Toast.makeText(ConnectToPcActivity.this,
                                            "Notice that we can't write in this folder, reading will work. Writing in subfolders might work.",
                                            Toast.LENGTH_LONG).show();
                                }
                                preference.setSummary(path);
                                stopServer();
                            }
                        }).setNegativeButton(R.string.cancel, null).create();
                folderPicker.show();
                return true;
            }
        });

        final CheckBoxPreference wakelock_pref = findPref("stayAwake");
        wakelock_pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                stopServer();
                return true;
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        updateRunningState();
        Log.d(Constants.TAG, "onResume: Register the preference change listner");
        SharedPreferences sp = getPreferenceScreen().getSharedPreferences();
        sp.registerOnSharedPreferenceChangeListener(this);
        Log.d(Constants.TAG, "onResume: Registering the FTP server actions");
        IntentFilter filter = new IntentFilter();
        filter.addAction(FsService.ACTION_STARTED);
        filter.addAction(FsService.ACTION_STOPPED);
        filter.addAction(FsService.ACTION_FAILEDTOSTART);
        registerReceiver(mFsActionsReceiver, filter);
    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.v(Constants.TAG, "onPause: Unregistering the FTPServer actions");
        unregisterReceiver(mFsActionsReceiver);
        Log.d(Constants.TAG, "onPause: Unregistering the preference change listner");
        SharedPreferences sp = getPreferenceScreen().getSharedPreferences();
        sp.unregisterOnSharedPreferenceChangeListener(this);
    }
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sp, String key) {
        updateLoginInfo();
    }
    private void startServer() {
        sendBroadcast(new Intent(FsService.ACTION_START_FTPSERVER));
    }
    private void stopServer() {
        sendBroadcast(new Intent(FsService.ACTION_STOP_FTPSERVER));
    }
    private void updateLoginInfo() {
        String username = FsSettings.getUserName();
        String password = FsSettings.getPassWord();
        Log.v(Constants.TAG, "Updating login summary");
        PreferenceScreen loginPreference = findPref("login");
        loginPreference.setSummary(username + " : " + transformPassword(password));
        ((BaseAdapter) loginPreference.getRootAdapter()).notifyDataSetChanged();
        EditTextPreference usernamePref = findPref("username");
        usernamePref.setSummary(username);
        EditTextPreference passWordPref = findPref("password");
        passWordPref.setSummary(transformPassword(password));
    }

    private void updateRunningState() {
        Resources res = getResources();
        TwoStatePreference runningPref = findPref("running_switch");
        if (FsService.isRunning()) {
            runningPref.setChecked(true);
            InetAddress address = FsService.getLocalInetAddress();
            if (address == null) {
                Log.v(Constants.TAG, "Unable to retrieve wifi ip address");
                runningPref.setSummary(R.string.cant_get_url);
                return;
            }
            String iptext = "ftp://" + address.getHostAddress() + ":"
                    + FsSettings.getPortNumber() + "/";
            String summary = res.getString(R.string.running_summary_started, iptext);
            runningPref.setSummary(summary);
        } else {
            runningPref.setChecked(false);
            runningPref.setSummary(R.string.running_summary_stopped);
        }
    }

    BroadcastReceiver mFsActionsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.v(Constants.TAG, "action received: " + intent.getAction());
            mHandler.removeCallbacksAndMessages(null);
            updateRunningState();
            final TwoStatePreference runningPref = findPref("running_switch");
            if (intent.getAction().equals(FsService.ACTION_FAILEDTOSTART)) {
                runningPref.setChecked(false);
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        runningPref.setSummary(R.string.running_summary_failed);
                    }
                }, 100);
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        runningPref.setSummary(R.string.running_summary_stopped);
                    }
                },5000);
            }
        }
    };

    static private String transformPassword(String password) {
        Context context = FsApp.getAppContext();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        Resources res = context.getResources();
        String showPasswordString = res.getString(R.string.show_password_default);
        boolean showPassword = showPasswordString.equals("true");
        showPassword = sp.getBoolean("show_password", showPassword);
        if (showPassword)
            return password;
        else {
            StringBuilder sb = new StringBuilder(password.length());
            for (int i = 0; i < password.length(); ++i)
                sb.append('*');
            return sb.toString();
        }
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    protected <T extends Preference> T findPref(CharSequence key) {
        return (T) this.findPreference(key);
    }
}
