package wwwfile.transfer.app.filetransferapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

import wwwfile.transfer.app.adapter.Adapter_PeersRecycler;
import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.base.DeviceListActivity;
import wwwfile.transfer.app.receiver.WiFiDirectBroadcastReceiver;
import wwwfile.transfer.app.services.FileTransferService;

public class ShowDeviceActivity extends DeviceListActivity{
    private RecyclerView rv_list_devices;
    private BroadcastReceiver receiver = null;
    private Intent intent;
    private List<String> selectedFiles=new LinkedList<String>();
    private final IntentFilter intentFilter = new IntentFilter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_device);
        intent=getIntent();
        if(intent.hasExtra("ListData")) {
            selectedFiles=intent.getStringArrayListExtra("ListData");
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        rv_list_devices=(RecyclerView)findViewById(R.id.recycler_view);
        rv_list_devices.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        wifiAdapter=new Adapter_PeersRecycler(this,peers);
        rv_list_devices.setAdapter(wifiAdapter);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
        manager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        channel = manager.initialize(this, getMainLooper(), null);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String localIP = Constants.getLocalIPAddress();
        String client_mac_fixed = new String(device.deviceAddress).replace("99", "19");
        String clientIP = Constants.getIPFromMac(client_mac_fixed);
        Uri uri = data.getData();
        TextView statusText = (TextView) findViewById(R.id.status_text);
        statusText.setText("Sending: " + uri);
        Intent serviceIntent = new Intent(this, FileTransferService.class);
        serviceIntent.setAction(FileTransferService.ACTION_SEND_FILE);
        serviceIntent.putExtra(FileTransferService.EXTRAS_FILE_PATH, uri.toString());
        if (localIP.equals(Constants.IP_SERVER)) {
            serviceIntent.putExtra(FileTransferService.EXTRAS_ADDRESS, clientIP);
        } else {
            serviceIntent.putExtra(FileTransferService.EXTRAS_ADDRESS, Constants.IP_SERVER);
        }
        serviceIntent.putExtra(FileTransferService.EXTRAS_PORT, Constants.PORT);
        startService(serviceIntent);
    }
    @Override
    public void onResume() {
        super.onResume();
        receiver = new WiFiDirectBroadcastReceiver(manager, channel, this);
        registerReceiver(receiver, intentFilter);
        onInitiateDiscovery();
        manager.discoverPeers(channel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Toast.makeText(ShowDeviceActivity.this, "Discovery Initiated", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onFailure(int reasonCode) {
                Toast.makeText(ShowDeviceActivity.this, "Discovery Failed: " + reasonCode, Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }
}