package wwwfile.transfer.app.filetransferapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import java.util.LinkedList;
import java.util.List;

import wwwfile.transfer.app.adapter.Adapter_Home_Items;
import wwwfile.transfer.app.models.HomeItemIcon;

public class HomePageActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private Adapter_Home_Items adapter;
    private List<HomeItemIcon> allItems;
    private RecyclerView recycler_view;
    private Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        allItems=new LinkedList<HomeItemIcon>();
        allItems.add(new HomeItemIcon(1, "Camera",R.drawable.ic_photo_camera_pink_a200_48dp , new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent=new Intent(getApplicationContext(),ExtendedMainActivity.class);
                intent.putExtra("tabid",0);
                startActivity(intent);
            }
        }));
        allItems.add(new HomeItemIcon(2, "Gallery",R.drawable.ic_image_pink_a200_48dp , new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent=new Intent(getApplicationContext(),ExtendedMainActivity.class);
                intent.putExtra("tabid",1);
                startActivity(intent);
            }
        }));
        allItems.add(new HomeItemIcon(3, "Music",R.drawable.ic_library_music_pink_a200_48dp , new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent=new Intent(getApplicationContext(),ExtendedMainActivity.class);
                intent.putExtra("tabid",2);
                startActivity(intent);
            }
        }));
        allItems.add(new HomeItemIcon(4, "Videos", R.drawable.ic_slow_motion_video_pink_a200_48dp, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent=new Intent(getApplicationContext(),ExtendedMainActivity.class);
                intent.putExtra("tabid",3);
                startActivity(intent);
            }
        }));
        allItems.add(new HomeItemIcon(5, "All Files", R.drawable.ic_folder_pink_a200_48dp, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent=new Intent(getApplicationContext(),ExtendedMainActivity.class);
                intent.putExtra("tabid",4);
                startActivity(intent);
            }
        }));
        recycler_view= ((RecyclerView) findViewById(R.id.recycler_view));
        recycler_view.setLayoutManager(new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL));
        adapter=new Adapter_Home_Items(HomePageActivity.this,allItems);
        recycler_view.setAdapter(adapter);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
        } else if (id == R.id.nav_gallery) {
        } else if (id == R.id.nav_slideshow) {
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
