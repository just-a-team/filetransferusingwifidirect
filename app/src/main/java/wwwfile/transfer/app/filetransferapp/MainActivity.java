package wwwfile.transfer.app.filetransferapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

import wwwfile.transfer.app.adapter.Adapter_ShowDataINTabs;
import wwwfile.transfer.app.fragments.FileListFragment;
import wwwfile.transfer.app.fragments.GalleryListFragment;
import wwwfile.transfer.app.fragments.MusicListFragment;
import wwwfile.transfer.app.fragments.PhotosListFragment;
import wwwfile.transfer.app.fragments.VideoListFragment;
import wwwfile.transfer.app.models.FragmentTitles;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static List<String> selectedFiles = new LinkedList<String>();
    public Intent intent;
    private FloatingActionButton fab_send;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        selectedFiles.clear();
        ViewPager viewPager = (ViewPager) findViewById(R.id.all_shared_data_viewPager);
        List<FragmentTitles> fragmentTitles = new LinkedList<FragmentTitles>();
//        fragmentTitles.add(new FragmentTitles("Apps", AppListFragment.newInstance()));
        fragmentTitles.add(new FragmentTitles("Camera", PhotosListFragment.newInstance()));
        fragmentTitles.add(new FragmentTitles("Gallery", GalleryListFragment.newInstance()));
        fragmentTitles.add(new FragmentTitles("Music", MusicListFragment.newInstance()));
        fragmentTitles.add(new FragmentTitles("Videos", VideoListFragment.newInstance()));
        fragmentTitles.add(new FragmentTitles("All Files", FileListFragment.newInstance()));
        PagerAdapter adapter = new Adapter_ShowDataINTabs(getSupportFragmentManager(), fragmentTitles);
        viewPager.setAdapter(adapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.all_main_tabs);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(getIntent().getIntExtra("tabid", 0));
        fab_send = (FloatingActionButton) findViewById(R.id.fab_send);
        fab_send.setOnClickListener(this);

    }
/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater as=new MenuInflater(this);
        as.inflate(R.menu.menu_main, menu);
        return true;
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_send:
                if(selectedFiles.size()>0) {
                    Intent intent = new Intent(this, ShowDeviceActivity.class);
                    intent.putExtra("ListData", selectedFiles.toArray());
                    startActivity(intent);
                }else {
                    Toast.makeText(MainActivity.this, "No Item Selected to send. ", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }
    public static void addItemtoList(String path) {
        if (!selectedFiles.contains(path)) {
            selectedFiles.add(path);
        }
    }
    public static void removeItemFromList(String path) {
        if (selectedFiles.contains(path)) {
            selectedFiles.remove(path);
        }
    }
}