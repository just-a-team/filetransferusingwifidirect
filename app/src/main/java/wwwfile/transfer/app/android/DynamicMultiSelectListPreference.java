package wwwfile.transfer.app.android;

import android.content.Context;
import android.preference.MultiSelectListPreference;
import android.util.AttributeSet;
import android.view.View;

public class DynamicMultiSelectListPreference extends MultiSelectListPreference {

    OnPopulateListener mPopulateListener;

    public DynamicMultiSelectListPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected View onCreateDialogView() {
        mPopulateListener.onPopulate(this);

        return super.onCreateDialogView();
    }

    public void setOnPopulateListener(OnPopulateListener listener) {
        mPopulateListener = listener;
    }

    public interface OnPopulateListener {
        void onPopulate(DynamicMultiSelectListPreference preference);
    }
}
