package wwwfile.transfer.app.models;

import android.view.View;

import java.io.Serializable;

public class HomeItemIcon implements Serializable{
    private int id;
    private String text;
    private int icon;
    private View.OnClickListener action;

    public HomeItemIcon(int id, String text, int icon,View.OnClickListener action) {
        this.id = id;
        this.text = text;
        this.icon = icon;
        this.action=action;
    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public int getIcon() {
        return icon;
    }

    public View.OnClickListener getAction() {
        return action;
    }
}
