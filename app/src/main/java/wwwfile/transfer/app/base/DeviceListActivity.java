package wwwfile.transfer.app.base;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

import wwwfile.transfer.app.adapter.Adapter_PeersRecycler;
import wwwfile.transfer.app.filetransferapp.R;
import wwwfile.transfer.app.interfaces.DeviceActionListener;
import wwwfile.transfer.app.tasks.ServerAsyncTask;

public class DeviceListActivity extends AppCompatActivity implements WifiP2pManager.ChannelListener, DeviceActionListener,WifiP2pManager.PeerListListener,WifiP2pManager.ConnectionInfoListener{
    protected boolean isWifiP2pEnabled = false;
    protected List<WifiP2pDevice> peers = new LinkedList<WifiP2pDevice>();
    protected ProgressDialog progressDialog = null;
    protected WifiP2pDevice firstdevice,device;
    protected Adapter_PeersRecycler wifiAdapter;
    protected WifiP2pManager manager;
    protected boolean retryChannel = false;
    protected WifiP2pManager.Channel channel;
    protected WifiP2pInfo info;

    public void setIsWifiP2pEnabled(boolean isWifiP2pEnabled) {
        this.isWifiP2pEnabled = isWifiP2pEnabled;
    }
    public void resetData() {
        clearPeers();
    }
    public void clearPeers() {
        peers.clear();
        wifiAdapter.notifyDataSetChanged();
    }
    public void onInitiateDiscovery() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        progressDialog = ProgressDialog.show(this, "Press back to cancel", "finding peers", true, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
            }
        });
    }
    public void updateThisDevice(WifiP2pDevice device) {
        this.firstdevice = device;
    }
    public WifiP2pDevice getDevice() {
        return firstdevice;
    }

    @Override
    public void onChannelDisconnected() {
        if (manager != null && !retryChannel) {
            Toast.makeText(this, "Channel lost. Trying again", Toast.LENGTH_LONG).show();
            resetData();
            retryChannel = true;
            manager.initialize(this, getMainLooper(), this);
        } else {
            Toast.makeText(this, "Severe! Channel is probably lost premanently. Try Disable/Re-Enable P2P.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onConnectionInfoAvailable(WifiP2pInfo info) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        this.info = info;
        findViewById(R.id.layout_connected).setVisibility(View.VISIBLE);
        TextView view = (TextView) findViewById(R.id.group_owner);
        view.setText(getResources().getString(R.string.group_owner_text) + ((info.isGroupOwner == true) ? getResources().getString(R.string.yes) : getResources().getString(R.string.no)));
        view = (TextView) findViewById(R.id.device_info);
        view.setText("Group Owner IP - " + info.groupOwnerAddress.getHostAddress());
        findViewById(R.id.btn_start_client).setVisibility(View.VISIBLE);
        new ServerAsyncTask(this, findViewById(R.id.status_text)).execute();
        findViewById(R.id.btn_connect).setVisibility(View.GONE);
    }

    @Override
    public void showDetails(WifiP2pDevice device) {
        showDeviceDetails(device);
    }
    public void showDeviceDetails(WifiP2pDevice device) {
        this.device = device;
        findViewById(R.id.layout_connected).setVisibility(View.VISIBLE);
        TextView view = (TextView) findViewById(R.id.device_address);
        view.setText(device.deviceAddress);
        view = (TextView) findViewById(R.id.device_info);
        view.setText(device.toString());
    }

    @Override
    public void cancelDisconnect() {
        if (manager != null) {
            if (getDevice() == null || getDevice().status == WifiP2pDevice.CONNECTED) {
                disconnect();
            } else if (getDevice().status == WifiP2pDevice.AVAILABLE || getDevice().status == WifiP2pDevice.INVITED) {
                manager.cancelConnect(channel, new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {
                        Toast.makeText(DeviceListActivity.this, "Aborting connection", Toast.LENGTH_SHORT).show();
                    }
                    @Override
                    public void onFailure(int reasonCode) {
                        Toast.makeText(DeviceListActivity.this, "Connect abort request failed. Reason Code: " + reasonCode, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }

    @Override
    public void connect(WifiP2pConfig config) {
        manager.connect(channel, config, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
            }

            @Override
            public void onFailure(int reason) {
                Toast.makeText(DeviceListActivity.this, "Connect failed. Retry.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void disconnect() {
        manager.removeGroup(channel, new WifiP2pManager.ActionListener() {
            @Override
            public void onFailure(int reasonCode) {
                Log.e(Constants.TAG, "Disconnect failed. Reason :" + reasonCode);
            }

            @Override
            public void onSuccess() {
            }
        });
    }

    @Override
    public void onPeersAvailable(WifiP2pDeviceList peers) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        this.peers.clear();
        this.peers.addAll(peers.getDeviceList());
        wifiAdapter.notifyDataSetChanged();
        if (this.peers.size() == 0) {
            Log.e(Constants.TAG, "No devices found");
            return;
        }
    }
}
