package wwwfile.transfer.app.base;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.net.wifi.p2p.WifiP2pDevice;
import android.provider.MediaStore;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import wwwfile.transfer.app.models.MusicItem;
import wwwfile.transfer.app.models.VideoItem;

public class Constants extends ConstantParams{

    public static List<File> getFilesList(File file)
    {
        List<File> fileList = new ArrayList<File>();
        File[] files = file.listFiles();
        fileList.clear();
        for (File f : files) {
            if (f.isDirectory()) {
                fileList.addAll(getFilesList(f));
            } else {
                fileList.add(f);
            }
        }
        return fileList;
    }

    public static List<File> getAllOtherFiles(Context context,File source)
    {
        List<String> videos=new LinkedList<String>();
        for (VideoItem item:getVideoList(context)) {
            videos.add(item.getData());
        }
        List<String> audios=new LinkedList<String>();
        for (MusicItem item:getMusicList(context)) {
            audios.add(item.getData());
        }
        List<String> allMediaData=new LinkedList<String>();
        allMediaData.addAll(videos);
        allMediaData.addAll(audios);
        allMediaData.addAll(getGalleryImages(context));
        allMediaData.addAll(getCameraImages(context));
        List<File> myFiles=new LinkedList<File>();
        for (String path:allMediaData) {
            myFiles.add(new File(path));
        }
        List<File> allFiles=getFilesList(source);
        allFiles.removeAll(myFiles);
        return allFiles;
    }
    public static List<VideoItem> getVideoList(Context context)
    {
        String[] projection = {
                MediaStore.Video.Media._ID,
                MediaStore.Video.Media.ARTIST,
                MediaStore.Video.Media.TITLE,
                MediaStore.Video.Media.DATA,
                MediaStore.Video.Media.DISPLAY_NAME,
                MediaStore.Video.Media.DURATION,
                MediaStore.Video.Media.SIZE
        };
        final Cursor cursor = context.getContentResolver().query(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                projection,
                null,
                null,
                null);
        List<VideoItem> songs = new ArrayList<VideoItem>();
        while(cursor.moveToNext()) {
            songs.add(new VideoItem(cursor.getString(0),cursor.getString(3),
                    cursor.getString(2),cursor.getString(1),cursor.getString(6),
                    cursor.getString(4),cursor.getString(5)));
        }
        cursor.close();
        return songs;
    }
    public static List<MusicItem> getMusicList(Context context)
    {
        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";

        String[] projection = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.SIZE
        };
        final Cursor cursor = context.getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                null,
                null);

        List<MusicItem> songs = new ArrayList<MusicItem>();

        while(cursor.moveToNext()) {
            songs.add(new MusicItem(cursor.getString(0),cursor.getString(3),
                    cursor.getString(2),cursor.getString(1),cursor.getString(6),
                    cursor.getString(4),cursor.getString(5)));
        }
        cursor.close();
        return songs;
    }
    public static List<String> getGalleryImages(Context context) {
        final String[] projection = { MediaStore.Images.Media.DATA };
        final Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                projection,
                null,
                null,
                null);
        ArrayList<String> result = new ArrayList<String>(cursor.getCount());
        if (cursor.moveToFirst()) {
            final int dataColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            do {
                final String data = cursor.getString(dataColumn);
                result.add(data);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }

    public static List<String> getCameraImages(Context context) {
        final String[] projection = { MediaStore.Images.Media.DATA };
        final String selection = MediaStore.Images.Media.BUCKET_ID + " = ?";
        final String[] selectionArgs = { Constants.getBucketId() };
        final Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                selectionArgs,
                null);
        ArrayList<String> result = new ArrayList<String>(cursor.getCount());
        if (cursor.moveToFirst()) {
            final int dataColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            do {
                final String data = cursor.getString(dataColumn);
                result.add(data);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }
    public static String getBucketId() {
        return String.valueOf(CAMERA_IMAGE_BUCKET_NAME.toLowerCase().hashCode());
    }
    public static String ConvertSizeInStandard(long sizeinBytes)
    {
        if(sizeinBytes>=SIZE_FACTOR) {
            if(sizeinBytes>=SIZE_FACTOR*SIZE_FACTOR) {
                if(sizeinBytes>=SIZE_FACTOR*SIZE_FACTOR*SIZE_FACTOR) {
                    return (sizeinBytes/(SIZE_FACTOR*SIZE_FACTOR*SIZE_FACTOR))+"GB";
                }else {
                    return (sizeinBytes/(SIZE_FACTOR*SIZE_FACTOR))+"MB";
                }
            }else {
                return (sizeinBytes/SIZE_FACTOR)+"KB";
            }
        }else{
            return sizeinBytes+" B";
        }
    }
    public static boolean isSystemPackage(PackageInfo pkgInfo) {
        return ((pkgInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) ? true : false;
    }
    public static String getDeviceStatus(int deviceStatus) {
        Log.e(Constants.TAG, "Peer status :" + deviceStatus);
        switch (deviceStatus) {
            case WifiP2pDevice.AVAILABLE:
                return "Available";
            case WifiP2pDevice.INVITED:
                return "Invited";
            case WifiP2pDevice.CONNECTED:
                return "Connected";
            case WifiP2pDevice.FAILED:
                return "Failed";
            case WifiP2pDevice.UNAVAILABLE:
                return "Unavailable";
            default:
                return "Unknown";
        }
    }
    public static final String getIPFromMac(String MAC) {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("/proc/net/arp"));
            String line;
            while ((line = br.readLine()) != null) {
                String[] splitted = line.split(" +");
                if (splitted != null && splitted.length >= 4) {
                    String device = splitted[5];
                    if (device.matches(".*" +p2pInt+ ".*")){
                        String mac = splitted[3];
                        if (mac.matches(MAC)) {
                            return splitted[0];
                        }
                    }
                }
            }
        } catch (Exception ex) {
            Log.e(TAG, "getLocalIPAddress()", ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Log.e(TAG, "getLocalIPAddress()", ex);
            }
        }
        return null;
    }
    public static final String getLocalIPAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    String iface = intf.getName();
                    if(iface.matches(".*" +p2pInt+ ".*")){
                        if (inetAddress instanceof Inet4Address) {
                            return getDottedDecimalIP(inetAddress.getAddress());
                        }
                    }
                }
            }
        } catch (SocketException|NullPointerException ex) {
            Log.e(TAG, "getLocalIPAddress()", ex);
        }
        return null;
    }
    private static final String getDottedDecimalIP(byte[] ipAddr) {
        String ipAddrStr = "";
        for (int i=0; i<ipAddr.length; i++) {
            if (i > 0) {
                ipAddrStr += ".";
            }
            ipAddrStr += ipAddr[i]&0xFF;
        }
        return ipAddrStr;
    }
    public static boolean copyFile(InputStream inputStream, OutputStream out) {
        byte buf[] = new byte[1024];
        int len;
        try {
            while ((len = inputStream.read(buf)) != -1) {
                out.write(buf, 0, len);
            }
            out.close();
            inputStream.close();
        } catch (IOException e) {
            Log.e(TAG, e.toString());
            return false;
        }
        return true;
    }
}
