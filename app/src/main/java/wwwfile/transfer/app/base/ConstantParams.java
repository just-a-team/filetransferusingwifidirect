package wwwfile.transfer.app.base;

import android.os.Environment;

public class ConstantParams {
    public static final String TAG="File Transfer";
    public static final String p2pInt = "p2p-p2p0";
    public static final int PORT = 8988;
    public static final String IP_SERVER = "192.168.49.1";
    public static final String APP_PATH = "/data/app";
    public static final double SIZE_FACTOR = 1024.0;
    public static final String CAMERA_IMAGE_BUCKET_NAME = Environment.getExternalStorageDirectory().toString() + "/DCIM/Camera";
}
