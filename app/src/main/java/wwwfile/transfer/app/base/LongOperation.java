package wwwfile.transfer.app.base;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class LongOperation extends AsyncTask<String, Void, String> {
    private Context context;
    public LongOperation(Context context)
    {
        this.context=context;
    }
    @Override
    protected String doInBackground(String... params) {
        ServerSocket s = null;
        Socket incoming = null;
        try{
            s = new ServerSocket(Constants.PORT);
            String ip = (s.getInetAddress()).getHostAddress();
            CharSequence text = ip;
            int duration = Toast.LENGTH_LONG;
            Toast toast = Toast.makeText(context, text, duration);
            Thread.sleep(1000);
            toast.show();
            while(true){
                incoming = s.accept();
               // executor.execute(new ServerPI(incoming));
            }
        }
        catch(Exception e){
            System.out.println(e.toString());
            e.printStackTrace();
        }
        finally{
            try {
                if(incoming != null)incoming.close();
            }
            catch(IOException ignore) {
            }
            try {
                if (s!= null) {
                    s.close();
                }
            }
            catch(IOException ignore) {
            }
        }

        return "Executed";
    }

    @Override
    protected void onPostExecute(String result) {
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onProgressUpdate(Void... values) {
    }
}
