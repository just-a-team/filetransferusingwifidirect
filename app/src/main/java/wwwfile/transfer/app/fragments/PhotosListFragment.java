package wwwfile.transfer.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import wwwfile.transfer.app.adapter.Adapter_Camera_Images;
import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.filetransferapp.R;

public class PhotosListFragment extends Fragment {
    private RecyclerView recyclerView;
    private Adapter_Camera_Images camImagesAdapter;
    public static PhotosListFragment newInstance() {
        PhotosListFragment abc=new PhotosListFragment();
        return abc;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photo_list, container, false);
        recyclerView= (RecyclerView) rootView.findViewById(R.id.recycler_view_photolist);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2, GridLayoutManager.VERTICAL, false));
        camImagesAdapter=new Adapter_Camera_Images((AppCompatActivity)getActivity(),Constants.getCameraImages(getActivity()));
        recyclerView.setAdapter(camImagesAdapter);
        return rootView;
    }



}
