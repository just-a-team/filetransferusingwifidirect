package wwwfile.transfer.app.fragments;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.LinkedList;
import java.util.List;

import wwwfile.transfer.app.adapter.Adapter_Installed_Applications;
import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.filetransferapp.R;

public class AppListFragment extends Fragment {
    private RecyclerView recyclerView;
    private PackageManager packagemanager;
    private List<PackageInfo> allAppsonDevices=new LinkedList<PackageInfo>();
    private Adapter_Installed_Applications appAdapter;

    public static AppListFragment newInstance() {
        return new AppListFragment();
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_app_list, container, false);
        recyclerView= (RecyclerView) rootView.findViewById(R.id.recycler_view_applist);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),3, LinearLayoutManager.VERTICAL, false));
        packagemanager=getActivity().getPackageManager();
        for (PackageInfo pack:packagemanager.getInstalledPackages(PackageManager.GET_PERMISSIONS)) {
            if(!Constants.isSystemPackage(pack)) {
                allAppsonDevices.add(pack);
            }
        }
        appAdapter=new Adapter_Installed_Applications((AppCompatActivity)getActivity(),packagemanager,allAppsonDevices);
        recyclerView.setAdapter(appAdapter);
        return rootView;
    }
}
