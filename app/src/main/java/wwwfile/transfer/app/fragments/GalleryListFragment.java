package wwwfile.transfer.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import wwwfile.transfer.app.adapter.Adapter_Gallery_Images;
import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.filetransferapp.R;

public class GalleryListFragment extends Fragment {
    private RecyclerView recyclerView;
    private Adapter_Gallery_Images camImagesAdapter;
    public static GalleryListFragment newInstance() {
        GalleryListFragment abc=new GalleryListFragment();
        return abc;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_gallery_list, container, false);
        recyclerView= (RecyclerView) rootView.findViewById(R.id.recycler_view_gallerylist);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2, GridLayoutManager.VERTICAL, false));
        camImagesAdapter=new Adapter_Gallery_Images((AppCompatActivity)getActivity(), Constants.getGalleryImages(getActivity()));
        recyclerView.setAdapter(camImagesAdapter);
        return rootView;
    }



}