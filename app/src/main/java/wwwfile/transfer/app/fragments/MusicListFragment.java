package wwwfile.transfer.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import wwwfile.transfer.app.adapter.Adapter_Music_List;
import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.filetransferapp.R;

public class MusicListFragment extends Fragment {
    private RecyclerView recyclerView;
    private Adapter_Music_List camImagesAdapter;
    public static MusicListFragment newInstance() {
        MusicListFragment abc=new MusicListFragment();
        return abc;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_music_list, container, false);
        recyclerView= (RecyclerView) rootView.findViewById(R.id.recycler_view_musiclist);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        camImagesAdapter=new Adapter_Music_List((AppCompatActivity)getActivity(), Constants.getMusicList(getActivity()));
        recyclerView.setAdapter(camImagesAdapter);
        return rootView;
    }
}
