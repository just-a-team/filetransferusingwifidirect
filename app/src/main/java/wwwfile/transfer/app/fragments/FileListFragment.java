package wwwfile.transfer.app.fragments;

import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;

import wwwfile.transfer.app.adapter.Adapter_File_List;
import wwwfile.transfer.app.base.Constants;
import wwwfile.transfer.app.filetransferapp.R;

public class FileListFragment extends Fragment {
    private RecyclerView recyclerView;
    private Adapter_File_List camImagesAdapter;
    public static FileListFragment newInstance() {
        FileListFragment abc=new FileListFragment();
        return abc;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_files, container, false);
        recyclerView= (RecyclerView) rootView.findViewById(R.id.recycler_view_filelist);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        camImagesAdapter=new Adapter_File_List((AppCompatActivity)getActivity(), Constants.getAllOtherFiles(getActivity(),new File(Environment.getExternalStorageDirectory().getAbsolutePath())));
        recyclerView.setAdapter(camImagesAdapter);
        return rootView;
    }
}
